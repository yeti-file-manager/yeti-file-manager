#!/bin/bash
#
#  Copyright information
#
#      Copyright (C) 2011-2015 Arttu Liimola <arttu.liimola@gmail.com>
#
#  License
#
#      This file is part of Yeti.
#
#      This program is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <http://www.gnu.org/licenses/>.

declare -A MIMES
declare -A PLACES
declare -A RES

CreateList()
{
  MIMES["application-msword"]="application-vnd.oasis.opendocument.text"
  MIMES["application-pdf"]="file"
  MIMES["application-pgp-keys"]="file"
  MIMES["application-pgp-signature"]="file"
  MIMES["application-rss+xml"]="file"
  MIMES["application-rar"]="application-zip"
  MIMES["application-vnd.android.package-archive"]="file"
  MIMES["application-vnd.ms-excel"]="application-vnd.oasis.opendocument.spreadsheet"
  MIMES["application-vnd.ms-powerpoint"]="application-vnd.sun.xml.impress"
  MIMES["application-vnd.oasis.opendocument.database"]="file"
  MIMES["application-vnd.oasis.opendocument.formula"]="file"
  MIMES["application-vnd.oasis.opendocument.graphics"]="file"
  MIMES["application-vnd.oasis.opendocument.image"]="application-vnd.sun.xml.draw"
  MIMES["application-vnd.oasis.opendocument.spreadsheet"]="file"
  MIMES["application-vnd.oasis.opendocument.spreadsheet-template"]="file"
  MIMES["application-vnd.oasis.opendocument.text"]="file"
  MIMES["application-vnd.oasis.opendocument.text-master"]="application-vnd.oasis.opendocument.text"
  MIMES["application-vnd.oasis.opendocument.text-template"]="file"
  MIMES["application-vnd.openxmlformats-officedocument.wordprocessingml.document"]="application-vnd.oasis.opendocument.text"
  MIMES["application-vnd.stardivision.calc"]="application-vnd.oasis.opendocument.spreadsheet"
  MIMES["application-vnd.stardivision.draw"]="application-vnd.sun.xml.draw"
  MIMES["application-vnd.stardivision.math"]="application-vnd.oasis.opendocument.formula"
  MIMES["application-vnd.sun.xml.calc"]="application-vnd.oasis.opendocument.spreadsheet"
  MIMES["application-vnd.sun.xml.calc.template"]="application-vnd.oasis.opendocument.spreadsheet-template"
  MIMES["application-vnd.sun.xml.draw"]="file"
  MIMES["application-vnd.sun.xml.draw.template"]="file"
  MIMES["application-vnd.sun.xml.impress"]="file"
  MIMES["application-vnd.sun.xml.impress.template"]="file"
  MIMES["application-vnd.sun.xml.math"]="application-vnd.oasis.opendocument.formula"
  MIMES["application-vnd.sun.xml.writer"]="application-vnd.oasis.opendocument.text"
  MIMES["application-vnd.sun.xml.writer.global"]="application-vnd.oasis.opendocument.text"
  MIMES["application-vnd.sun.xml.writer.template"]="application-vnd.oasis.opendocument.text-template"
  MIMES["application-x-abiword"]="application-vnd.oasis.opendocument.text"
  MIMES["application-x-bittorrent"]="file"
  MIMES["application-x-cpio"]="application-x-tar"
  MIMES["application-x-debian-package"]="file"
  MIMES["application-x-gnumeric"]="application-vnd.oasis.opendocument.spreadsheet"
  MIMES["application-x-iso9660-image"]="file"
  MIMES["application-x-kword"]="application-vnd.oasis.opendocument.text"
  MIMES["application-x-lha"]="application-zip"
  MIMES["application-x-object"]="file"
  MIMES["application-x-shockwave-flash"]="file"
  MIMES["application-x-tar"]="file"
  MIMES["application-x-troff-man"]="file"
  MIMES["application-xhtml+xml"]="file"
  MIMES["application-zip"]="file"
  MIMES["audio-midi"]="file"
  MIMES["audio-prs.sid"]="audio-midi"
  MIMES["audio-x-aiff"]="audio-x-wav"
  MIMES["audio-x-generic"]="file"
  MIMES["audio-x-pn-realaudio"]="video-x-generic"
  MIMES["audio-x-realaudio"]="video-x-generic"
  MIMES["audio-x-wav"]="file"
  MIMES["image-svg+xml"]="file"
  MIMES["image-x-generic"]="file"
  MIMES["text-calendar"]="file"
  MIMES["text-comma-separated-values"]="file"
  MIMES["text-css"]="file"
  MIMES["text-html"]="file"
  MIMES["text-mathml"]="file"
  MIMES["text-plain"]="file"
  MIMES["text-rtf"]="application-vnd.oasis.opendocument.text"
  MIMES["text-x-bibtex"]="file"
  MIMES["text-x-chdr"]="file"
  MIMES["text-x-c++hdr"]="file"
  MIMES["text-x-csrc"]="file"
  MIMES["text-x-c++src"]="file"
  MIMES["text-x-generic"]="file"
  MIMES["text-x-haskell"]="file"
  MIMES["text-x-java"]="file"
  MIMES["text-x-pascal"]="file"
  MIMES["text-x-tcl"]="file"
  MIMES["text-x-tex"]="text-x-bibtex"
  MIMES["text-x-vcard"]="file"
  MIMES["text-xml"]="file"
  MIMES["unknown"]="file"
  MIMES["video-x-generic"]="file"
  MIMES["video-x-mng"]="image-x-generic"

  PLACES["folder-remote"]="file"
  PLACES["folder-root"]="file"
  PLACES["folder"]="file"
  PLACES["network"]="file"
  
  RES["new_bookmark"]="file"
  RES["folder_remote"]="file"
  RES["folder_root"]="file"
  RES["folder"]="file"
  RES["network"]="file"
  RES["sdcard"]="file"
}

CreateIcons()
{
  local W="$1"
  local H="$1"

  if [ ! -d "$W""x""$H" ]; then
    mkdir "$W""x""$H""/mimetypes" -p
    mkdir "$W""x""$H""/places"
  fi

  for i in "${!MIMES[@]}"
  do
    if [ "${MIMES[$i]}" == "file" ]
    then
      inkscape -f scalable/mimetypes/"$i".svgz -w="$W" -h="$H" -e "$W""x""$H""/mimetypes/""$i"".png"
    else
      ln -s "${MIMES[$i]}"".png" "$W""x""$H""/mimetypes/""$i"".png"
    fi
  done

  for i in "${!PLACES[@]}"
  do
    if [ "${PLACES[$i]}" == "file" ]
    then
      inkscape -f scalable/places/"$i".svgz -w="$W" -h="$H" -e "$W""x""$H""/places/""$i"".png"
    else
      ln -s "${PLACES[$i]}"".png" "$W""x""$H""/places/""$i"".png"
    fi
  done
}

CreateRes()
{
  local dpi="$1"
  local W="$2"
  local H="$2"

  if [ ! -d "res/drawable-""$dpi" ]; then
    mkdir "res/drawable-""$dpi" -p
  fi
  
  for i in "${!RES[@]}"
  do
    if [ "${RES[$i]}" == "file" ]
    then
      inkscape -f scalable/res/"$i".svgz -w="$W" -h="$H" -e "res/drawable-""$dpi""/""$i"".png"
    else
      ln -s "${RES[$i]}"".png" "res/drawable-""$dpi""/""$i"".png"
    fi
  done
}

Main()
{
  CreateList

  CreateIcons "32"
  CreateIcons "48"
  CreateIcons "64"
  CreateIcons "80"
  CreateIcons "96"
  CreateIcons "112"
  CreateIcons "128"
  CreateIcons "144"
  CreateIcons "160"
  CreateIcons "176"
  CreateIcons "192"
  
  CreateRes "ldpi" "36"
  CreateRes "mdpi" "48"
  CreateRes "hdpi" "72"
  CreateRes "xhdpi" "96"
  CreateRes "xxhdpi" "144"
  CreateRes "xxxhdpi" "192"
}

Main