/*
*  TabView.java
*
*  Copyright information
*
*      Copyright (C) 2011-2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import android.content.Context;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

/** 
 * Class for tab view.
 *
 * @author Arttu Liimola
 * @version 1.5
 * date: 22.01.2012
 */
public class TabView extends ViewGroup implements OnTouchListener, OnItemClickListener
{
	/** Display's width */
	private static int displayW;
	
	/** Max velocity */
	private static int maxXvelocity;
	
	/** Min velocity */
	private static int minXvelocity;
	
	/** Distances to start changing folder.*/
	private static int[] startChangingFolder;
	
	/** How much alpha is changed. */
	private static float changeAlpha;
	
	/**	Instance of SelectionView. */
	private static SelectionView selectionView;
	
	/** Interface to global information about an application environment. */
	private static Context context;
	
	/** View of tab content that contains all tabs. */
	private static View tabContent;
	
	/** Velocity tracker. */
	private VelocityTracker velocity;
	
	/** Folders. */
	private ArrayList<Folder> folders;
	
	/** Current folder. */
	private int currentFolder;
	
	/** Current distance in pixels that finger is moved. */
	private int currentW;
	
	/** Position where touch started. */
	private int startX;
	
	/** Old position of finger. */
	private int oldX;
	
	/** Direction of touch. */
	private int direction;
	
	/** View's width */
	private int viewW;
	
	/** View's height */
	private int viewH;
	
	/** Velocity */
	private int xVelocity;
	
	/** FolderView's scale. */
	private float scale;
	
	/** FolderView's alpha. */
	private float alpha;
	
	/** Show only folders. */
	private boolean showOnlyFolders;
	
	/** Is folder changing. */
	private boolean changingFolder;
	
	/** Touch direction changed. */
	private boolean directionChanged;
	
	/** Animation playing. */
	private boolean animationPlaying;
	
	/** Is selectionShown. */
	private boolean selectionShown;
	
	/** If true tab does not have header. */
	private boolean noHeader;
	
	/**
	 * Initializes static variables.
	 * 
	 * @param context       Interface to global information about an application environment.
	 * @param selectionView Instance of SelectionView.
	 * @param tabContent    View of tab content that contains all tabs.
	 */
	public static void set(Context context, SelectionView selectionView, View tabContent)
	{
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		int dip                = TypedValue.COMPLEX_UNIT_DIP;
		displayW               = metrics.widthPixels;
		maxXvelocity           = (int)TypedValue.applyDimension(dip, 20, metrics);
		minXvelocity           = (int)TypedValue.applyDimension(dip, 10, metrics);
		
		startChangingFolder    = new int[2];
		startChangingFolder[0] = (int)TypedValue.applyDimension(dip, 15, metrics);
		startChangingFolder[1] = displayW - startChangingFolder[0];
		changeAlpha            = 1f / startChangingFolder[1];
		TabView.tabContent     = tabContent;
		TabView.context        = context;
		TabView.selectionView  = selectionView;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param path Path to the folder in tab.
	 */
	public TabView(String path)
	{
		super(context);
		setAll(false);
		
		String[] allFolders = path.split("/");
		path                = "/";
    	addFolder(path);
		
		for (int i = 0; i < allFolders.length; i++)
		{
			if (!allFolders[i].equals(""))
			{
				path  += allFolders[i] + "/";
		    	addFolder(path);
			}
		}
	}
	
	/**
	 * Constructor.
	 * 
	 * @param folders Folders to open.
	 */
	public TabView(HashMap<String, Boolean> folders)
	{
		super(context);
		setAll(false);
		
		Iterator<String> foldersIt = folders.keySet().iterator();
		int realCurrentFolder      = 0;
		String folder;
		
		for (int i = 0; foldersIt.hasNext(); i++)
		{
			folder = foldersIt.next();
			addFolder(folder);
			
			if (folders.get(folder))
				realCurrentFolder = i;
		}
		
		FolderView currentChild     = (FolderView)getChildAt(currentFolder);
		FolderView realCurrentChild = (FolderView)getChildAt(realCurrentFolder);

		currentChild.layout(0, 0, 0, 0);
		realCurrentChild.setTransformation(1f, 1f, 0);
		realCurrentChild.layout(0, 0, viewW, viewH);
		
		currentFolder = realCurrentFolder;
	}
	
	/**
	 * Constructor when using tab in default folder setting.
	 * 
	 * @param path      Path to the folder in tab.
	 * @param inSetting If tab is used in default folder setting.
	 */
	public TabView(String path, boolean inSetting)
	{
		super(context);
		setAll(true);
		
		String[] allFolders = path.split("/");
		path                = "/";
    	addFolder(path);
		
		for (int i = 0; i < allFolders.length; i++)
		{
			if (!allFolders[i].equals(""))
			{
				path  += allFolders[i] + "/";
		    	addFolder(path);
			}
		}
	}

	/**
	 * Initializes variables for TabView.
	 * 
	 * @param inSetting If tab is used in default folder setting.
	 */
	public void setAll(boolean inSetting)
	{
		velocity          = null;
		folders           = new ArrayList<Folder>();
		currentW          = 0;
		startX            = 0;
		oldX              = 0;
		direction         = 0;
		viewW             = 0;
		viewH             = 0;
		xVelocity         = 0;
		scale             = 0f;
		alpha             = 0f;
		showOnlyFolders   = inSetting;
		noHeader          = inSetting;
		changingFolder    = false;
		directionChanged  = false;
		animationPlaying  = false;
		selectionShown    = false;
		
		setOnTouchListener(this);
	}
	
	/**
	 * Called when item is clicked.
	 * 
	 * @param folderView Item's parent.
	 * @param itemView   Item.
	 * @param pos        Item's position.
	 * @param id         Item's id.
	 */
	@Override
	public void onItemClick(AdapterView<?> folderView, View itemView, int pos, long id)
	{	
		Folder folder = folders.get(currentFolder);
		String name   = ((TextView)itemView.findViewById(R.id.name)).getText().toString();
		Item item     = folder.getItem(name);

		if (item != null)
		{	
			String mime = item.getMime();
			
			if (folder.itemExists(name))
			{
				if (mime == "folder")
				{
					addFolder(folder.getPath() + name  + "/");
					
					folder = folders.get(currentFolder);
					
					if (!noHeader)
						TabManager.changeHeader(folder.getName());
					
					if (!folder.canRead())
						Toast.makeText(context, R.string.cannotReadFolder, Toast.LENGTH_SHORT).show();
				}
				else
				{
					FileManagement.openFile(folder, name, mime);
				}
			}
			else
			{
				FolderManager.refresh(folders.get(currentFolder).getPath());
				
				Toast.makeText(context, R.string.itemDoesNotExist, Toast.LENGTH_SHORT).show();
			}
		}
		
	}
	
	/**
	 * This is called when there is motion event going to children.
	 * 
	 * @param event Motion event.
	 * 
	 * @return Return true if the event is handled in parent.
	 */
	@Override
    public boolean onInterceptTouchEvent(MotionEvent event)
	{
		boolean filter = false;

		if (!animationPlaying)
		{
			switch (event.getAction())
			{		
				case MotionEvent.ACTION_DOWN:
				{
					if (event.getPointerCount() < 2)
					{
						MainView.setDoNotChangeView(true);
						
						velocity = VelocityTracker.obtain();
						velocity.addMovement(event);
						actionDown((int)event.getX());
					}
					else
					{
						if (!changingFolder)
						{
							showSelection((int)event.getX(0), (int)event.getY(0), (int)event.getX(1), (int)event.getY(1));
							filter         = true;
						}
					}
					
					break;
				}
				
				case MotionEvent.ACTION_MOVE:
				{
					if (event.getPointerCount() < 2 || changingFolder)
					{
						selectionView.setBounds(0, 0, 0, 0);
						
						if (velocity != null && !selectionShown)
						{
							velocity.addMovement(event);
							filter = actionMove((int)event.getX());
						}
					}
					else
					{
						if (!changingFolder)
						{
							showSelection((int)event.getX(0), (int)event.getY(0), (int)event.getX(1), (int)event.getY(1));
							filter         = true;
						}
					}
					
					break;
				}
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_CANCEL:
				{
					if (!selectionShown)
						((FolderView)getChildAt(currentFolder)).clearSeletion();
					
					if (changingFolder && !selectionShown)
					{
						if (velocity != null)
						{
							velocity.addMovement(event);
							velocity.computeCurrentVelocity(1000, maxXvelocity);
							xVelocity = (int)velocity.getXVelocity();
		
							actionUp((int)event.getX());
						}
					}
					else
					{
						selectionShown = false;
						
						MainView.setDoNotChangeView(false);
						selectionView.setBounds(0, 0, 0, 0);
					}
					
					if (velocity != null)
					{
						velocity.recycle();
						velocity = null;
					}
					
					break;
				}
			}
		}
		
		return filter;
	}
	
	/**
	 * This is called when received motion event.
	 * 
	 * @param v     View that the touch event is dispatched to.
	 * @param event Motion event.
	 * 
	 * @return Returns true if event has been consumed.
	 */
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{	
		if (!animationPlaying)
		{
			switch (event.getAction())
			{
				case MotionEvent.ACTION_DOWN:
				{
					if (event.getPointerCount() < 2)
					{
						MainView.setDoNotChangeView(true);
						
						velocity = VelocityTracker.obtain();
						velocity.addMovement(event);
						actionDown((int)event.getX());
					}
					else
					{
						if (!changingFolder)
						{
							showSelection((int)event.getX(0), (int)event.getY(0), (int)event.getX(1), (int)event.getY(1));
						}
					}
					
					break;
				}
				case MotionEvent.ACTION_MOVE:
				{	
					if (event.getPointerCount() < 2 || changingFolder)
					{
						selectionView.setBounds(0, 0, 0, 0);
						
						if (velocity != null && !selectionShown)
						{
							velocity.addMovement(event);
							actionMove((int)event.getX());
						}
					}
					else
					{
						if (!changingFolder)
						{
							showSelection((int)event.getX(0), (int)event.getY(0), (int)event.getX(1), (int)event.getY(1));
						}
					}
					
					break;
				}
			
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_CANCEL:
				{
					if (!selectionShown)
						((FolderView)getChildAt(currentFolder)).clearSeletion();
					
					if (changingFolder && !selectionShown)
					{
						if (velocity != null)
						{
							velocity.addMovement(event);
							velocity.computeCurrentVelocity(1000, maxXvelocity);
							xVelocity = (int)velocity.getXVelocity();
		
							actionUp((int)event.getX());
						}
					}
					else
					{
						selectionShown = false;
						
						MainView.setDoNotChangeView(false);
						selectionView.setBounds(0, 0, 0, 0);
					}
					
					if (velocity != null)
					{
						velocity.recycle();
						velocity = null;
					}
					
					break;
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Get folders.
	 * 
	 * @return Folders.
	 */
	public ArrayList<Folder> getFolders()
	{
		return folders;
	}
	
	/**
	 * Get tab's header.
	 * 
	 * @return Returns tab's header.
	 */
	public String getHeader()
	{
		return folders.get(currentFolder).getName();
	}
	
	/**
	 * Get current folder.
	 * 
	 * @return Returns current folder.
	 */
	public Folder getCurrentFolder()
	{
		return folders.get(currentFolder);
	}
	
	/**
	 * Get current folder's path.
	 * 
	 * @return Returns current folder's path.
	 */
	public String getCurrentPath()
	{
		return folders.get(currentFolder).getPath();
	}
	
	/**
	 * Get selected items.
	 * 
	 * @return Selected items.
	 */
	public ArrayList<String> getSelectedItems()
	{
		return ((FolderView)getChildAt(currentFolder)).getSelectedItems();
	}
	
	/**
	 * Move to previous folder.
	 * 
	 * @return Returns true if moving was possible.
	 */
	public boolean movePreviousFolder()
	{
		boolean move = false;
		
		if (currentFolder > 0)
		{
			FolderView currentChild = (FolderView)getChildAt(currentFolder);
			FolderView prevChild    = (FolderView)getChildAt(currentFolder - 1);
			
			currentChild.clearSeletion();

			currentChild.layout(0, 0, 0, 0);
			prevChild.setTransformation(1f, 1f, 0);
			prevChild.layout(0, 0, viewW, viewH);
			currentFolder--;
			
			move = true;
		}
		
		return move;
	}
	
	/**
	 * Measure view's and its children's measured width and height.
	 * 
	 * @param widthMeasureSpec  Horizontal space requirement from parent.
	 * @param heightMeasureSpec Vertical space requirement from parent.
	 */
	@Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
        int count = getChildCount();
        viewW     = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        viewH     = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
		
        for (int i = 0; i < count; i++)
        	getChildAt(i).measure(widthMeasureSpec, heightMeasureSpec);
        
        setMeasuredDimension(viewW, viewH);
    }
	
	/**
	 * Called when view group should set size and position to its children's.
	 * 
	 * @param changed New position or size.
	 * @param l       Left position.
	 * @param t       Top position.
	 * @param r       Right position.
	 * @param b       Bottom position.
	 */
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b)
	{
		int count = getChildCount();
		FolderView child;

        for (int i = 0; i < count; i++)
        {
            child = (FolderView)getChildAt(i);

            if (i == currentFolder)
            {
            	child.layout(0, 0, viewW, viewH);
            }
            else
            {
            	child.layout(0, 0, 0, 0);
            }
        }

    }
	
	/**
	 * Adds folder to tab.
	 * 
	 * @param path Folder's path.
	 */
	private void addFolder(String path)
	{
		boolean add = false;

		if ((currentFolder + 1) < getChildCount())
		{
			String nextPath = folders.get(this.currentFolder + 1).getPath();
			
			if (!nextPath.equals(path))
			{
				Iterator<Folder> foldersIt = folders.iterator();
				Folder rmFolder;
			
				for (int i = 0; foldersIt.hasNext(); i++)
				{
					rmFolder = foldersIt.next();
					
					if (i > currentFolder)
					{
						removeView(rmFolder.removeView(this));
						foldersIt.remove();
					}
				}
				
				add = true;
			}
		}
		else
		{
			add = true;
		}
		
		if (add)
		{
			Folder folder         = FolderManager.createFolder(path, showOnlyFolders);
			FolderView folderView = folder.getView(this);
			folderView.setOnItemClickListener(this);
			currentFolder = getChildCount();
			folders.add(folder);
			addView(folderView);
		}
		else
		{	
			FolderView currentChild = (FolderView)getChildAt(currentFolder);
			FolderView nextChild    = (FolderView)getChildAt(currentFolder + 1);

			currentChild.layout(0, 0, 0, 0);
			nextChild.setTransformation(1f, 1f, 0);
			nextChild.layout(0, 0, viewW, viewH);
			currentFolder++;
		}
	}
	
	/**
	 * Handles touch down event.
	 * 
	 * @param newX X position of touch.
	 */
	private void actionDown(int newX)
	{
		direction      = 0;
		startX         = newX;
		changingFolder = false;
	}
	
	/**
	 * Handles touch move event.
	 * 
	 * @param newX X position of touch.
	 * 
	 * @return Returns true if event is consumed.
	 */
	private boolean actionMove(int newX)
	{
		boolean filter = false;
		
		if (!changingFolder)
		{
			if (newX > startChangingFolder[0] && newX < startChangingFolder[1])	
			{
				boolean ok = false;
				
				if (newX > startX && newX - startX > startChangingFolder[0])
					ok = true;
				else if (newX < startX && startX - newX > startChangingFolder[0])
					ok = true;
				
				if (ok)
				{
					changingFolder    = true;
					direction = newX > startX ? 1 : -1;
					alpha     = newX > startX ? 0 : 1;
					oldX      = startX;
					startX    = newX;
					
					if (currentFolder - direction > -1 && currentFolder - direction < getChildCount())
					{
						if (direction == 1)
						{
							FolderView prevChild = (FolderView)getChildAt(currentFolder - 1);
							prevChild.layout(0, 0, viewW, viewH);
							prevChild.setTransformation(1f, 0f, 0);
						}
						else if (direction == -1)
						{
							FolderView currentChild = (FolderView)getChildAt(currentFolder);
							FolderView nextChild    = (FolderView)getChildAt(currentFolder + 1);
							currentChild.layout(0, 0, viewW, viewH);
							nextChild.setTransformation(1f, 1f, 0);
							nextChild.layout(0, 0, viewW, viewH);
						}
					}
				}
			}
		}
		
		if (direction == 1 && currentFolder > 0)
		{
			filter   = true;
			showPrevFolder(newX);
		}
		else if (direction == -1 && currentFolder < (folders.size() - 1))
		{
			filter   = true;
			showNextFolder(newX);
		}
		
		return filter;
	}
	
	/**
	 * Show file selection.
	 * 
	 * @param firstX  X position of first touchpoint.
	 * @param firstY  Y position of first touchpoint.
	 * @param secondX X position of second touchpoint.
	 * @param secondY Y position of second touchpoint.
	 */
	private void showSelection(int firstX, int firstY, int secondX, int secondY)
	{
		int left   = 0;
		int top    = 0;
		int right  = 0;
		int bottom = 0;
		int disX   = Math.abs(firstX - secondX);
		int disY   = Math.abs(firstY - secondY);
		
		if (!selectionShown)
		{
			int showSelection = ResourcesManager.<Integer>getSetting("MainViewChange");
			
			if (disX < showSelection && disY < showSelection)
			{
				MainView.setDoNotChangeView(true);
				
				selectionShown = true;
			}
			else
			{
				MainView.setDoNotChangeView(false);
			}
		}
		
		if (selectionShown)
		{
			int tabContentTop    = tabContent.getTop();
			int minSelectionRect = ResourcesManager.<Integer>getSetting("MinSelectionRect");
			int minSelectionHalf = minSelectionRect / 2;
			
			if (firstX > secondX)
			{
				left  = secondX;
				right = firstX;
			}
			else
			{
				left  = firstX;
				right = secondX;
			}

			if (firstY > secondY)
			{
				top    = secondY;
				bottom = firstY;
			}
			else
			{
				top    = firstY;
				bottom = secondY;
			}
			
			if (disY > 10 || disX > 10)
			{
				Rect bounds   = selectionView.getBounds();
				bounds.top    = bounds.top - tabContentTop;
				bounds.bottom = bounds.bottom - tabContentTop;
				
				if (disY < 10)
				{
					top    = bounds.top > 0 ? bounds.top : top - minSelectionHalf;
					bottom = bounds.bottom > 0 ? bounds.bottom : bottom + minSelectionHalf;
				}
			
				if (disX < 10)
				{
					left  = bounds.left > 0 ? bounds.left : left - minSelectionHalf;
					right = bounds.right > 0 ? bounds.right : right + minSelectionHalf;
				}
				
				selectionView.setBounds(left, top + tabContentTop, right, bottom + tabContentTop);
				((FolderView)getChildAt(currentFolder)).selection(left, top, right, bottom);
			}
		}
	}
	
	/**
	 * Handles touch up event.
	 * 
	 * @param newX X position of touch.
	 */
	private void actionUp(int newX)
	{
		if (changingFolder)
		{
			Integer params[] = {newX};
			
			if ((direction == 1 && currentFolder > 0)
				|| (direction == -1 && currentFolder < (folders.size() - 1)))
				new FolderAnimation().execute(params);
		}
	}
	
	/**
	 * Show previous folder.
	 * 
	 * @param newX X position of touch.
	 */
	private void showPrevFolder(int newX)
	{
		FolderView currentChild = (FolderView)getChildAt(currentFolder);
		FolderView prevChild    = (FolderView)getChildAt(currentFolder - 1);
		
		currentW = newX > startX ? newX - startX : 0;
		scale    = 1;
		
		currentChild.setTransformation(1f, 1f, currentW);
		
		if (oldX < newX)
		{
			directionChanged = false;

			if (currentW > startChangingFolder[0])
			{
				scale = calculateScale();
				alpha = calculateAlpha(newX - oldX, true);
				
				prevChild.setTransformation(scale, alpha, 0);
			}
		}
		else
		{
			if (currentW < (displayW / 2))
				directionChanged = true;

			if (currentW > startChangingFolder[0])
			{
				scale = calculateScale();
				alpha = calculateAlpha(oldX - newX, false);
				
				prevChild.setTransformation(scale, alpha, 0);
			}
			else
			{
				alpha = 0f;
				
				prevChild.setTransformation(scale, alpha, 0);
			}
		}
		
		oldX = newX;
	}
	
	/**
	 * Show next folder.
	 * 
	 * @param newX X position of touch.
	 */
	private void showNextFolder(int newX)
	{
		FolderView currentChild = (FolderView)getChildAt(currentFolder);
		FolderView nextChild    = (FolderView)getChildAt(currentFolder + 1);
		
		currentW = newX < startX ? displayW - (startX - newX) : displayW;
		scale    = 1;

		nextChild.setTransformation(1f, 1f, currentW);
		
		if (oldX > newX)
		{
			directionChanged = false;

			if (currentW < startChangingFolder[1])
			{
				scale = calculateScale();
				alpha = calculateAlpha(oldX - newX, false);
				
				currentChild.setTransformation(scale, alpha, 0);
			}
		}
		else
		{
			if (currentW > (displayW / 2))
				directionChanged = true;

			if (currentW < startChangingFolder[1])
			{
				scale = calculateScale();
				alpha = calculateAlpha(newX - oldX, true);
				
				currentChild.setTransformation(scale, alpha, 0);
			}
			else
			{
				alpha = 1f;
				
				currentChild.setTransformation(scale, alpha, 0);
			}
		}
		
		oldX = newX;
	}
	
	/**
	 * Calculates scale for folder view.
	 * 
	 * @return Returns folder's scale.
	 */
	private float calculateScale()
	{
		return (0.3f * ((float)currentW / (float)startChangingFolder[1])) + 0.7f;
	}
	
	/**
	 * Calculates alpha for folder view.
	 * 
	 * @return Returns folder's alpha.
	 */
	private float calculateAlpha(float x, boolean increase)
	{
		float newAlpha;
		
		if (increase)
			newAlpha = alpha + (changeAlpha * x);
		else
			newAlpha = alpha - (changeAlpha * x);

		return newAlpha;
	}
	
	/** 
	 * Class for changing folder animation.
	 *
	 * @author Arttu Liimola
	 * @version 1.0
	 * date: 21.12.2011
	 */
	private class FolderAnimation extends AsyncTask<Integer, Void, Void>
    {
		/** current folder view */
		private FolderView currentChild = (FolderView)getChildAt(currentFolder);
		
		/** next or previous folder view */
		private FolderView npChild      = (FolderView)getChildAt(currentFolder - direction);	
		
		/**
		 * Calculates animation steps.
		 * 
		 * @param params X position of touch.
		 */
		@Override
		protected Void doInBackground(Integer... params)
		{
			animationPlaying   = true;
			int newX           = params[0];
			int oneStep        = Math.abs(xVelocity);
			oneStep            = oneStep < minXvelocity ? minXvelocity : oneStep;
			int durationOfStep = 20;
			long time          = System.currentTimeMillis();
			long newTime       = time + durationOfStep;
			
			if ((direction == 1 && !directionChanged) || (direction == -1 && directionChanged))
			{
				while (currentW < displayW)
				{
					if ((newTime - time) >= durationOfStep)
					{
						currentW = currentW + oneStep;
						oldX     = newX;
						newX     = newX + 20;
						scale    = calculateScale();
						alpha    = calculateAlpha(newX - oldX, true);
						
						if (alpha > 1f)
							alpha = 1f;
	
						time = newTime;
						
						publishProgress();
					}
	
					newTime = System.currentTimeMillis();
				}
			}
			else if ((direction == -1 && !directionChanged) || (direction == 1 && directionChanged))
			{
				while (currentW > 0)
				{
					if ((newTime - time) >= durationOfStep)
					{
						currentW = currentW - oneStep;
						oldX     = newX;
						newX     = newX - 20;
						scale    = calculateScale();
						alpha    = calculateAlpha(oldX - newX, false);
						
						if (alpha < 0f)
							alpha = 0f;
	
						time = newTime;
						
						publishProgress();
					}
	
					newTime = System.currentTimeMillis();
				}
			}
			
			return null;
		}
		
		/**
		 * Displays animation steps.
		 */
		@Override
		protected void onProgressUpdate(Void... n)
		{
			if (direction == 1)
			{	
				currentChild.setTransformation(1f, 1f, currentW);
				npChild.setTransformation(scale, alpha, 0);
			}
			else if (direction == -1)
			{		
				currentChild.setTransformation(scale, alpha, 0);
				npChild.setTransformation(1f, 1f, currentW);
			}
		}
		
		/**
		 * Called after animation is ready.
		 */
		protected void onPostExecute(Void n)
		{
			if (direction == 1 && !directionChanged)
					currentFolder--;
			else if (direction == -1 && !directionChanged)
					currentFolder++;

			if (!directionChanged)
			{
				npChild.setTransformation(1f, 1f, 0);
				currentChild.layout(0, 0, 0, 0);
				npChild.layout(0, 0, viewW, viewH);
			}
			else
			{
				npChild.layout(0, 0, 0, 0);
				currentChild.setTransformation(1f, 1f, 0);
			}
			
			Folder folder = folders.get(currentFolder);
			
			if (!noHeader)
				TabManager.changeHeader(folder.getName());
			
			if (!folder.canRead())
				Toast.makeText(context, R.string.cannotReadFolder, Toast.LENGTH_SHORT).show();
			
			direction        = 0;
			animationPlaying = false;
			
			MainView.setDoNotChangeView(false);
	    }
    }
}
