/*
*  FolderShowPreviewsPreference.java
*
*  Copyright information
*
*      Copyright (C) 2011-2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import android.content.Context;
import android.preference.CheckBoxPreference;
import android.util.AttributeSet;

/** 
 * Class for toggling preview showing.
 *
 * @author Arttu Liimola
 * @version 1.1
 * date: 05.01.2012
 */
public class FolderShowPreviewsPreference extends CheckBoxPreference
{
	/**
	 * Constructor.
	 * 
	 * @param context Interface to global information about an application environment.
	 * @param attrs   Collection of attributes from XML-file.
	 */
	public FolderShowPreviewsPreference(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		
		setChecked(TabManager.getCurrentTab().getCurrentFolder().showsPreviews());
	}

	/**
	 * Called when user clicks preference.
	 */
	public void onClick()
	{
		super.onClick();
		
		FolderManager.setShowPreviews(TabManager.getCurrentTab().getCurrentPath(), isChecked());
	}
}
