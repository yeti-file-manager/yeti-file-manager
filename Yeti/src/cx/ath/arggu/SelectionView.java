/*
*  SelectionView.java
*
*  Copyright information
*
*      Copyright (C) 2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import java.util.ArrayList;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.view.View;

/** 
 * Class for drawing selection rectangular.
 *
 * @author Arttu Liimola
 * @version 1.1
 * date: 29.01.2012
 */
public class SelectionView extends View
{
	/**	Selection rectangular. */
	private ShapeDrawable rect;
	
	/**	Selection rectangular borders. */
	private ArrayList<Line> lines;
	
	/**	Selection rectangular borders paint.  */
	private Paint linesPaint;

	/**
	 * Constructor.
	 * 
	 * @param context Interface to global information about an application environment.
	 * @param attrs   Collection of attributes from XML-file.
	 */
	public SelectionView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		rect       = new ShapeDrawable(new RectShape());
		lines      = new ArrayList<Line>();
		linesPaint = new Paint();
		
		rect.getPaint().setColor(context.getResources().getColor(R.color.selectingRect));
		linesPaint.setColor(context.getResources().getColor(R.color.selectingRectBorders));
		
		lines.add(new Line(0, 0, 0, 0));
		lines.add(new Line(0, 0, 0, 0));
		lines.add(new Line(0, 0, 0, 0));
		lines.add(new Line(0, 0, 0, 0));
	}

	/**
	 * Sets rectangular bounds.
	 * 
	 * @param left   Left position.
	 * @param top    Top position.
	 * @param right  Right position.
	 * @param bottom Bottom position.
	 */
	public void setBounds(int left, int top, int right, int bottom)
	{
		rect.setBounds(left, top, right, bottom);
		
		lines.get(0).setBounds(left, top, right, top);
		lines.get(1).setBounds(right, top, right, bottom);
		lines.get(2).setBounds(left, bottom, right, bottom);
		lines.get(3).setBounds(left, top, left, bottom);
		
		invalidate();
	}
	
	/**
	 * Get rectangular bounds.
	 * 
	 * @return Rectangular bounds
	 */
	public Rect getBounds()
	{
		return rect.getBounds();
	}
	
	/**
	 * Draws view.
	 * 
	 * @param canvas The Canvas to which the view will be drawn.
	 */
	@Override  
	protected void onDraw(Canvas canvas)
	{  
		super.onDraw(canvas);
		
		rect.draw(canvas);
		
		int[] bounds = lines.get(0).getBounds();
		canvas.drawLine(bounds[0], bounds[1], bounds[2], bounds[3], linesPaint);
		
		bounds = lines.get(1).getBounds();
		canvas.drawLine(bounds[0], bounds[1], bounds[2], bounds[3], linesPaint);
		
		bounds = lines.get(2).getBounds();
		canvas.drawLine(bounds[0], bounds[1], bounds[2], bounds[3], linesPaint);
		
		bounds = lines.get(3).getBounds();
		canvas.drawLine(bounds[0], bounds[1], bounds[2], bounds[3], linesPaint);
	}  
	
	/** 
	 * Class for selection rectangular borders.
	 *
	 * @author Arttu Liimola
	 * @version 1.0
	 * date: 05.01.2012
	 */
	private class Line
	{
		/** Line's bounds. */
		private int[] bounds;
		
		/**
		 * Constructor.
		 * 
		 * @param startX Start x position.
		 * @param startY Start y position.
		 * @param stopX  Stop x position.
		 * @param stopY  Stop y position.
		 */
		public Line(int startX, int startY, int stopX, int stopY)
		{
			bounds    = new int[4];
			bounds[0] = startX;
			bounds[1] = startY;
			bounds[2] = stopX;
			bounds[3] = stopY;
		}
		
		/**
		 * Set line's bounds.
		 * 
		 * @param startX Start x position.
		 * @param startY Start y position.
		 * @param stopX  Stop x position.
		 * @param stopY  Stop y position.
		 */
		public void setBounds(int startX, int startY, int stopX, int stopY)
		{
			bounds[0] = startX;
			bounds[1] = startY;
			bounds[2] = stopX;
			bounds[3] = stopY;
		}
		
		/**
		 * Get line's bounds.
		 * 
		 * @return Line's bounds.
		 */
		public int[] getBounds()
		{
			return bounds;
		}
		
	}
	
}
