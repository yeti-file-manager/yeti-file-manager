/*
*  MainViewChangePreference.java
*
*  Copyright information
*
*      Copyright (C) 2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

/** 
 * Class for setting distance for fingers to change main view dialog preference.
 *
 * @author Arttu Liimola
 * @version 1.0
 * date: 29.01.2011
 */
public class MainViewChangePreference extends DialogPreference implements OnSeekBarChangeListener
{
	/** Interface to global information about an application environment. */
	private Context context;
	
	/** View that show value to user. */
	private Toast toast;
	
	/** Seekbar that is used to change distance for fingers to change main view. */
	private SeekBar seekBar;
	
	/**	Minimum pixel value for the distance. */
	private int mainViewChangeMin;
	
	/**	Single seekbar step in pixels. */
	private int stepInPx;
	
	/**
	 * Constructor.
	 * 
	 * @param context Interface to global information about an application environment.
	 * @param attrs   Collection of attributes from XML-file.
	 */
	public MainViewChangePreference(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		
		this.context = context;
		this.toast   = null;
	}

	/**
	 * Called when seekbar value is changed.
	 * 
	 * @param seekbar  Seekbar thats values is changed.
	 * @param progress Seekbar's value.
	 * @param fromUser Specifies if the value was changed by user.
	 */
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
	{
		int mainViewChange = (progress * stepInPx) + mainViewChangeMin;
		String msg         = "Distance: " + Integer.toString(mainViewChange) + "px";	
		
		if (toast != null)
			toast.setText(msg);
		else
			toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);

		toast.show();
	}

	/**
	 * Called when user starts touch gesture on seekbar.
	 * 
	 * @param seekbar Seekbar in which the touch gesture began.
	 */
	@Override
	public void onStartTrackingTouch(SeekBar seekBar)
	{		
	}

	/**
	 * Called when user finishes touch gesture on seekbar.
	 * 
	 * @param seekbar Seekbar in which the touch gesture finishes.
	 */
	@Override
	public void onStopTrackingTouch(SeekBar seekBar)
	{
	}
	
	/**
	 * Creates content view for dialog.
	 * 
	 * @return Content view for dialog.
	 */
	@Override
	protected View onCreateDialogView()
	{
		LayoutInflater li      = LayoutInflater.from(context);
		View layout            = li.inflate(R.layout.settings_main_view_change, null);
		seekBar                = (SeekBar)layout.findViewById(R.id.settingsMainViewChangeSlider);
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		int dip                = TypedValue.COMPLEX_UNIT_DIP;
		int mainViewChange     = ResourcesManager.<Integer>getSetting("MainViewChange");
		mainViewChangeMin      = (int)TypedValue.applyDimension(dip, 100, metrics);
		stepInPx               = (int)TypedValue.applyDimension(dip, 200, metrics) / 20;
		
		seekBar.setMax(20);
		seekBar.setProgress((mainViewChange - mainViewChangeMin) / stepInPx);
		seekBar.setOnSeekBarChangeListener(this);
		
		return layout;
	}
	
	/**
	 * Called when dialog is closed, saves setting on positive result.
	 * 
	 * @param positiveResult True if user clicked ok.
	 */
	@Override
	protected void onDialogClosed(boolean positiveResult)
	{
		if (positiveResult)
		{
			int mainViewChange = (seekBar.getProgress() * stepInPx) + mainViewChangeMin;

			if (mainViewChange != ResourcesManager.<Integer>getSetting("MainViewChange"))
			{		
				ResourcesManager.setSetting("MainViewChange", mainViewChange);
				
				MainView.refreshSettings();
			}
		}
			
	}
}
