/*
*  MainView.java
*
*  Copyright information
*
*      Copyright (C) 2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import android.content.Context;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

/** 
 * Class for main view.
 *
 * @author Arttu Liimola
 * @version 1.1
 * date: 29.01.2012
 */
public class MainView extends ViewGroup implements OnTouchListener
{
	/** If true do not change view. */
	private static boolean doNotChangeView = false;
	
	/**	Current view. */
	private static Views currentView;
	
	/**	Distance that determines if view is changed.  */
	private static int changeView;
	
	/**	ENUM for views. */
	private enum Views
	{
		PlacesMenu,
		FileManagement
	}
	
	/**	Places menu view. */
	private LinearLayout placesMenu;
	
	/**	File management view. */
	private FrameLayout fileManagement;
	
	/**	View width. */
	private int viewW;
	
	/**	View height. */
	private int viewH;
	
	/**	X position where touch started. */
	private int startX;
	
	/** Old X position of finger. */
	private int oldX;
	
	/** Direction of the touch. */
	private int direction;
	
	/**	True if direction of the touch changed. */
	private boolean directionChanged;
	
	/** Velocity tracker. */
	private VelocityTracker velocity;
	
	/** Max velocity */
	private int maxXvelocity;
	
	/** Min velocity */
	private int minXvelocity;
	
	/** Velocity */
	private int xVelocity;
	
	/**
	 * Constructor.
	 * 
	 * @param context Interface to global information about an application environment.
	 * @param attrs   Collection of attributes from XML-file.
	 */
	public MainView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		
		if (placesMenu == null)
		{
			placesMenu     = (LinearLayout)findViewById(R.id.placesMenu);
			fileManagement = (FrameLayout)findViewById(R.id.fileManagement);
		}
		
		startX                 = -1;
		oldX                   = -1;
		changeView             = ResourcesManager.<Integer>getSetting("MainViewChange");
		direction              = 0;
		directionChanged       = false;
		currentView            = Views.FileManagement;
		velocity               = null;
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		int dip                = TypedValue.COMPLEX_UNIT_DIP;
		maxXvelocity           = (int)TypedValue.applyDimension(dip, 20, metrics);
		minXvelocity           = (int)TypedValue.applyDimension(dip, 10, metrics);
		xVelocity              = 0;
		
		setOnTouchListener(this);
	}

	/**
	 * Set do not change view.
	 * 
	 * @param doNotChangeView If true view will not be changed.
	 */
	public static void setDoNotChangeView(boolean doNotChangeView)
	{
		MainView.doNotChangeView = doNotChangeView;
	}
	
	/**
	 * Called when view is changed via other method than gesture.
	 */
	public static void viewChanged()
	{
		if (currentView == Views.FileManagement)
			currentView = Views.PlacesMenu;
		else
			currentView = Views.FileManagement;
	}
	
	/**
	 * Called when settings that affect this class are changed.
	 */
	public static void refreshSettings()
	{
		changeView = ResourcesManager.<Integer>getSetting("MainViewChange");
	}
	
	/**
	 * This is called when there is motion event going to children.
	 * 
	 * @param event Motion event.
	 * 
	 * @return Return true if the event is handled in parent.
	 */
	@Override
    public boolean onInterceptTouchEvent(MotionEvent event)
	{
		boolean filter = false;
		
		if (!doNotChangeView)
		{
			switch (event.getAction())
			{
				case MotionEvent.ACTION_DOWN:
				case MotionEvent.ACTION_MOVE:
				{	
					if (event.getPointerCount() > 1)
					{
						filter = changeView(event);
					}
					
					break;
				}
			
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_CANCEL:
				{
					if (velocity != null)
					{
						velocity.addMovement(event);
						velocity.computeCurrentVelocity(1000, maxXvelocity);
						xVelocity = (int)velocity.getXVelocity();
						velocity.recycle();
						velocity = null;
	
						Integer params[] = {(int)event.getX()};
						
						new ViewAnimation().execute(params);
					}
					break;
				}
			}
		}
		
		return filter;
	}
	

	/**
	 * This is called when received motion event.
	 * 
	 * @param v     View that the touch event is dispatched to.
	 * @param event Motion event.
	 * 
	 * @return Returns true if event has been consumed.
	 */
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		if (!doNotChangeView)
		{
			switch (event.getAction())
			{
				case MotionEvent.ACTION_DOWN:
				case MotionEvent.ACTION_MOVE:
				{	
					if (event.getPointerCount() > 1)
					{
						changeView(event);
					}
					
					break;
				}
			
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_CANCEL:
				{
					if (velocity != null)
					{
						velocity.addMovement(event);
						velocity.computeCurrentVelocity(1000, maxXvelocity);
						xVelocity = (int)velocity.getXVelocity();
						velocity.recycle();
						velocity = null;
						
						Integer params[] = {(int)event.getX()};
						
						new ViewAnimation().execute(params);
						
					}
					break;
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Measure view's and its children's measured width and height.
	 * 
	 * @param widthMeasureSpec  Horizontal space requirement from parent.
	 * @param heightMeasureSpec Vertical space requirement from parent.
	 */
	@Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		if (placesMenu == null)
		{
			placesMenu     = (LinearLayout)findViewById(R.id.placesMenu);
			fileManagement = (FrameLayout)findViewById(R.id.fileManagement);
		}
		
        viewW = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        viewH = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
		
        placesMenu.measure(widthMeasureSpec, heightMeasureSpec);
        fileManagement.measure(widthMeasureSpec, heightMeasureSpec);
        
        setMeasuredDimension(viewW, viewH);
    }
	
	/**
	 * Called when view group should set size and position to its children's.
	 * 
	 * @param changed New position or size.
	 * @param l       Left position.
	 * @param t       Top position.
	 * @param r       Right position.
	 * @param b       Bottom position.
	 */
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b)
	{
		if (placesMenu == null)
		{
			placesMenu     = (LinearLayout)findViewById(R.id.placesMenu);
			fileManagement = (FrameLayout)findViewById(R.id.fileManagement);
		}
		
		placesMenu.layout(0, 0, 0, 0);
		fileManagement.layout(0, 0, viewW, viewH);
	}

	/**
	 * Changes view.
	 * 
	 * @param firstX  X position of first touchpoint.
	 * @param firstY  Y position of first touchpoint.
	 * @param secondX X position of second touchpoint.
	 * @param secondY Y position of second touchpoint.
	 */
	private boolean changeView(MotionEvent event)
	{	
		boolean filter = false;
		
		int firstX  = (int)event.getX(0);
		int firstY  = (int)event.getY(0);
		int secondX = (int)event.getX(1);
		int secondY = (int)event.getY(1);
		int disX    = Math.abs(firstX - secondX);
		int disY    = Math.abs(firstY - secondY);
		
		if (disX >= changeView || disY >= changeView)
		{
			velocity = velocity == null ? VelocityTracker.obtain() : velocity;
			velocity.addMovement(event);
			
			filter = true;
			startX = startX == -1 ? firstX : startX;
			oldX   = oldX == -1 ? startX : oldX;
			
			if (currentView == Views.FileManagement && startX < firstX)
			{
				int x  = firstX - startX;
				
				placesMenu.layout(x - viewW, 0, x, viewH);
				fileManagement.layout(x, 0, viewW + x, viewH);
				
				direction        = 1;
				directionChanged = x < (viewW / 2) && oldX > firstX;
				
				oldX = firstX;
			}
			else if (currentView == Views.PlacesMenu && startX > firstX)
			{
				int x  = startX - firstX;
				
				placesMenu.layout(-x, 0, viewW - x, viewH);
				fileManagement.layout(viewW - x, 0, 2 * viewW - x, viewH);
				
				direction        = -1;
				directionChanged = x < (viewW / 2) && oldX < firstX;
				
				oldX = firstX;
			}
		}
		
		return filter;
	}
	
	/** 
	 * Class for changing view animation.
	 *
	 * @author Arttu Liimola
	 * @version 1.0
	 * date: 03.01.2012
	 */
	private class ViewAnimation extends AsyncTask<Integer, Void, Void>
    {
		
		/** X position. */
		private int x;
		
		/**
		 * Calculates animation steps.
		 * 
		 * @param params X position of touch.
		 */
		@Override
		protected Void doInBackground(Integer... params)
		{
			x                  = params[0];
			int oneStep        = Math.abs(xVelocity);
			oneStep            = oneStep < minXvelocity ? minXvelocity : oneStep;
			int durationOfStep = 20;
			long time          = System.currentTimeMillis();
			long newTime       = time + durationOfStep;
			
			if ((direction == 1 && !directionChanged)
				|| (direction == -1 && directionChanged))
			{
				while (x < viewW)
				{
					if ((newTime - time) >= durationOfStep)
					{
						x  = x + oneStep;
	
						time = newTime;
						
						publishProgress();
					}
	
					newTime = System.currentTimeMillis();
				}
			}
			else if ((direction == -1 && !directionChanged)
					 || (direction == 1 && directionChanged))
			{
				while (x > 0)
				{
					if ((newTime - time) >= durationOfStep)
					{
						x = x - oneStep;
	
						time = newTime;
						
						publishProgress();
					}
	
					newTime = System.currentTimeMillis();
				}
			}
			
			return null;
		}
		
		/**
		 * Displays animation steps.
		 */
		@Override
		protected void onProgressUpdate(Void... n)
		{
			placesMenu.layout(-(viewW - x), 0, x, viewH);
			fileManagement.layout(x, 0, (viewW + x), viewH);
		}
		
		/**
		 * Called after animation is ready.
		 */
		protected void onPostExecute(Void n)
		{
			if ((direction == 1 && !directionChanged)
				|| (direction == -1 && directionChanged))
			{
				placesMenu.layout(0, 0, viewW, viewH);
				fileManagement.layout(0, 0, 0, 0);
				
				currentView = Views.PlacesMenu;
			}
			else if ((direction == -1 && !directionChanged)
					 || (direction == 1 && directionChanged))
			{
				placesMenu.layout(0, 0, 0, 0);
				fileManagement.layout(0, 0, viewW, viewH);
				
				currentView = Views.FileManagement;
			}
			
			startX           = -1;
			oldX             = -1;
			direction        = 0;
			directionChanged = false; 
		}
    }
}
