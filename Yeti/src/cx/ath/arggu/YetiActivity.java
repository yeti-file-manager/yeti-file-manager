/*
*  YetiActivity.java
*
*  Copyright information
*
*      Copyright (C) 2011-2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TabHost;

/**
 * Class for main activity.
 *
 * @author Arttu Liimola
 * @version 1.7
 * date: 29.01.2012
 */
public class YetiActivity extends Activity
{
	/**	Places menu view. */
	private LinearLayout placesMenu;
	
	/** Container for tabbed window view. */
	private TabHost tabHost;
	
	/** Close tab menu item. **/
	private MenuItem menuCloseTab;
	
	/** Create folder menu item. **/
	private MenuItem menuCreateFolder;
	
	/** Cut menu item. **/
	private MenuItem menuCut;
	
	/** Copy menu item. **/
	private MenuItem menuCopy;
	
	/** Paste menu item. **/
	private MenuItem menuPaste;
	
	/** Delete menu item. **/
	private MenuItem menuDelete;
	
    /**
     * Called when the activity is first created.
     * 
     * @param savedInstanceState If activity is re-initialized this contains data set in
     * onSaveInstanceState.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ResourcesManager.set(this);
        ClipboardManager.set();
        FileManagement.set(this);
        setContentView(R.layout.main);
        
        placesMenu = (LinearLayout)findViewById(R.id.placesMenu);
        
        tabHost = (TabHost)findViewById(R.id.tabHost);
        tabHost.setup();
        
        ArrayList<HashMap<String, Boolean>> tabs = null;
        int currentTab                           = -1;
        
        if (savedInstanceState != null && savedInstanceState.containsKey("Tabs")
        	&& savedInstanceState.containsKey("CurrentTab"))
        {
        	tabs = (ArrayList<HashMap<String, Boolean>>)savedInstanceState.getSerializable("Tabs");
        	
        	currentTab = savedInstanceState.getInt("CurrentTab");
        }

        FolderManager.set();
        Folder.set(this);
        BookmarkManager.set(this, placesMenu);
        TabView.set(this, (SelectionView)findViewById(R.id.selectionView),
        			tabHost.findViewById(android.R.id.tabcontent));
        TabManager.set(tabHost, tabs, currentTab);
    }
    
    /**
     * Initializes the contents of the menu.
     * 
     * @param menu Instance of Menu in which the menu items are placed.
     * 
     * @return True to display the menu.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        menuCloseTab     = menu.findItem(R.id.closeTab);
        menuCreateFolder = menu.findItem(R.id.createFolder);
        menuCut          = menu.findItem(R.id.cutItems);
        menuCopy         = menu.findItem(R.id.copyItems);
        menuPaste        = menu.findItem(R.id.pasteItems);
        menuDelete       = menu.findItem(R.id.deleteItems);
        
        menuCut.setEnabled(false);
        
        return true;
    }
    
    /**
     * Called right before menu is shown.
     * 
     * @param menu Menu that was initialized in onCreateOptionsMenu.
     * 
     * @return True to display the menu.
     */
    @Override
    public boolean onPrepareOptionsMenu (Menu menu)
    {
    	Folder currentFolder = TabManager.getCurrentTab().getCurrentFolder();
    	
    	menuCloseTab.setEnabled(TabManager.getTabs().size() > 1);
    	menuCreateFolder.setEnabled(currentFolder.canWrite());
    	
    	if (!TabManager.getCurrentTab().getSelectedItems().isEmpty())
    	{
    		//menuCut.setEnabled(currentFolder.canWrite());
    		menuCopy.setEnabled(true);
    		menuDelete.setEnabled(currentFolder.canWrite());
    	}
    	else
    	{
    		//menuCut.setEnabled(false);
    		menuCopy.setEnabled(false);
    		menuDelete.setEnabled(false);
    	}
    	
    	if (!ClipboardManager.getItems().isEmpty())
    	{
    		Folder from = ClipboardManager.getFolder();
    		
        	if (!from.equals(currentFolder))
        	{
        		menuPaste.setEnabled(currentFolder.canWrite());
        	}
        	else
        	{
        		menuPaste.setEnabled(false);
        	}
    	}
    	else
    	{
    		menuPaste.setEnabled(false);
    	}
    	
    	return true;
    }
    
    /**
     * This is called when user selects item in menu.
     * 
     * @param item Menu item that user selected.
     * 
     * @return Return true if the selection is handled.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
    	boolean handle = false;
    	
        switch (item.getItemId())
        {
        	case R.id.openSettings:
        	{
        		Intent intent = new Intent(this, SettingsActivity.class);
        		startActivity(intent);
        		handle = true;
        		
        		break;
        	}
        
        	case R.id.folderOptions:
        	{
        		Intent intent = new Intent(this, FolderOptionsActivity.class);
        		startActivity(intent);
        		handle = true;
        		
        		break;
        	}
        	
	        case R.id.newTab:
	        {
	        	TabManager.createTab(ResourcesManager.<String>getSetting("DefaultPath"));
	        	handle = true;
	        	
	        	break;
	        }
	        
	        case R.id.closeTab:
	        {
	        	TabManager.closeTab();
	        	handle = true;
	        	
	        	break;
	        }
	        
	        case R.id.refreshFolder:
	        {
	        	FolderManager.refresh(TabManager.getCurrentTab().getCurrentPath());
	        	handle = true;
	        	
	        	break;
	        }
	        
	        case R.id.createFolder:
	        {
	        	FileManagement.createFolder(TabManager.getCurrentTab().getCurrentPath());
	        	handle = true;
	        	
	        	break;
	        }
	        
	        case R.id.cutItems:
	        {
	        	ClipboardManager.setItems(TabManager.getCurrentTab().getSelectedItems());
	        	ClipboardManager.setFolder(TabManager.getCurrentTab().getCurrentFolder());
	        	ClipboardManager.setCutItems(true);
	        	handle = true;
	        	
	        	break;
	        }
	        
	        case R.id.copyItems:
	        {
	        	ClipboardManager.setItems(TabManager.getCurrentTab().getSelectedItems());
	        	ClipboardManager.setFolder(TabManager.getCurrentTab().getCurrentFolder());
	        	ClipboardManager.setCutItems(false);
	        	handle = true;
	        	
	        	break;
	        }
	        
	        case R.id.pasteItems:
	        {
	        	Folder from             = ClipboardManager.getFolder();
	        	Folder to               = TabManager.getCurrentTab().getCurrentFolder();
	        	ArrayList<String> items = ClipboardManager.getItems();
		        	
	        	if (!ClipboardManager.getCutItems())
	        	{
		        	FileManagement.copy(from, to, items);
	        	}
	        	
	        	handle = true;

	        	break;
	        }
	        
	        case R.id.deleteItems:
	        {
	        	final Folder from             = TabManager.getCurrentTab().getCurrentFolder();
	        	final ArrayList<String> items = TabManager.getCurrentTab().getSelectedItems();
	        	
	        	String msg = "Are you sure you want to delete " + items.size() + " items from " + from.getName() + " ?";
	        	
	        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
	        	builder.setMessage(msg);
	        	builder.setCancelable(false);
	        	builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
	        	{
	        		public void onClick(DialogInterface dialog, int id)
	        		{
	        			Folder clipboardFolder = ClipboardManager.getFolder();
	        			
	        			if (clipboardFolder != null && clipboardFolder.equals(from))
	        				ClipboardManager.deleteItems(items);
	        			
	        			FileManagement.delete(from , items);
	        	    }
	        	});
	        	builder.setNegativeButton("No", new DialogInterface.OnClickListener()
	        	{
	        		public void onClick(DialogInterface dialog, int id)
	        		{
	        			dialog.cancel();
	        		}
	        	});
	        	AlertDialog alert = builder.create();
	        	
	        	alert.show();

	        	handle = true;
	        }
        }
        
        return handle;
    }
    
    /**
     * Called when user press back button.
     */
    @Override
    public void onBackPressed()
    {
    	if (!TabManager.getCurrentTab().movePreviousFolder())
    	{
    		super.onBackPressed();
    	}
    }
    
    /**
     * Called when activity is about to be killed to save state.
     * 
     * @param savedInstanceState Bundle to which state is saved.
     */
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState)
    {
    	super.onSaveInstanceState(savedInstanceState);
    	
    	Iterator<TabView> tabsIt                 = TabManager.getTabs().iterator();
    	ArrayList<HashMap<String, Boolean>> tabs = new ArrayList<HashMap<String, Boolean>>();
    	HashMap<String, Boolean> folderList;
    	ArrayList<Folder> folders;
    	Iterator<Folder> foldersIt;
    	TabView tab;
    	Folder folder;
    	String path;
    	String currentPath;
    	
    	while (tabsIt.hasNext())
    	{
    		tab           = tabsIt.next();
    		folders       = tab.getFolders();
    		foldersIt     = folders.iterator();
    		folderList    = new HashMap<String, Boolean>();
    		currentPath   = tab.getCurrentPath();
    		
    		while (foldersIt.hasNext())
    		{
    			folder = foldersIt.next();
    			path   = folder.getPath();
    			
    			if (path.equals(currentPath))
    				folderList.put(path, true);
    			else
    				folderList.put(path, false);
    		}
    		
    		tabs.add(folderList);
    	}
    	
    	savedInstanceState.putSerializable("Tabs", tabs);
    	savedInstanceState.putInt("CurrentTab", TabManager.getCurrentTabIndex());
    }
}