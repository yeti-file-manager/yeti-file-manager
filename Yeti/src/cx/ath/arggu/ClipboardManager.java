/*
*  ClipboardManager.java
*
*  Copyright information
*
*      Copyright (C) 2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import java.util.ArrayList;
import java.util.Iterator;

/** 
 * Class for managing clipboard.
 *
 * @author Arttu Liimola
 * @version 1.1
 * date: 16.01.2012
 */
public class ClipboardManager
{
	/** Items to copy or move. **/
	private static ArrayList<String> items;
	
	/** Folder where items are located. **/
	private static Folder folder;
	
	/** True if items were cut. **/
	private static boolean cutItems;
	
	/**
	 * Initializes variables of this class.
	 */
	public static void set()
	{
		items    = new ArrayList<String>();
		folder   = null;
		cutItems = false;
	}
	
	/**
	 * Set items.
	 * 
	 * @param items Items to copy or move.
	 */
	public static void setItems(ArrayList<String> items)
	{
		ClipboardManager.items = new ArrayList<String>(items);
	}
	
	/**
	 * Set folder where items are located.
	 * 
	 * @param folder Folder where items are located.
	 */
	public static void setFolder(Folder folder)
	{
		ClipboardManager.folder = folder;
	}
	
	/**
	 * Set if items were cut.
	 * 
	 * @param cutItems True if items were cut.
	 */
	public static void setCutItems(boolean cutItems)
	{
		ClipboardManager.cutItems = cutItems;
	}
	
	/**
	 * Get items to copy or move.
	 * 
	 * @return Items to copy or move.
	 */
	public static ArrayList<String> getItems()
	{
		return items;
	}
	
	/**
	 * Get folder where items are located.
	 * 
	 * @return Folder where items are located.
	 */
	public static Folder getFolder()
	{
		return folder;
	}
	
	/**
	 * Get if items were cut.
	 * 
	 * @return True if items were cut.
	 */
	public static boolean getCutItems()
	{
		return cutItems;
	}
	
	/**
	 * Delete items from list that were deleted.
	 * 
	 * @param items Items that were deleted.
	 */
	public static void deleteItems(ArrayList<String> items)
	{
		Iterator<String> itemsIt = items.iterator();
		
		while(itemsIt.hasNext())
			ClipboardManager.items.remove(itemsIt.next());
	}
}
