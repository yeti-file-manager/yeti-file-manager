/*
*  Bookmark.java
*
*  Copyright information
*
*      Copyright (C) 2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

/** 
 * Class for bookmark.
 *
 * @author Arttu Liimola
 * @version 1.0
 * date: 22.01.2012
 */
public class Bookmark
{
	/** Bookmark's name. **/
	private String name;
	
	/** Bookmark's target. **/
	private String path;
	
	/** Bookmark's icon. **/
	private String icon;
	
	/**
	 * Constructor.
	 * 
	 * @param name Bookmark's name.
	 * @param path Bookmark's target
	 * @param icon Bookmark's icon.
	 */
	public Bookmark(String name, String path, String icon)
	{
		this.name = name;
		this.path = path;
		this.icon = icon;
	}
	
	/**
	 * Get bookmark's name.
	 * 
	 * @return Bookmarks's name.
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Get bookmark's target.
	 * 
	 * @return Bookmarks's target.
	 */
	public String getPath()
	{
		return path;
	}
	
	/**
	 * Get bookmark's icon.
	 * 
	 * @return Bookmarks's icon.
	 */
	public String getIcon()
	{
		return icon;
	}
}
