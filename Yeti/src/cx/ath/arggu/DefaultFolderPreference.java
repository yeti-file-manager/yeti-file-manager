/*
*  DefaultFolderPreference.java
*
*  Copyright information
*
*      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;

/** 
 * Class for default folder dialog preference.
 *
 * @author Arttu Liimola
 * @version 1.1
 * date: 31.12.2011
 */
public class DefaultFolderPreference extends DialogPreference
{
	/** Dialogs content view */
	private TabView tab;
	
	/**
	 * Constructor.
	 * 
	 * @param context Interface to global information about an application environment.
	 * @param attrs   Collection of attributes from XML-file.
	 */
	public DefaultFolderPreference(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	/**
	 * Creates content view for dialog.
	 * 
	 * @return Content view for dialog.
	 */
	@Override
	protected View onCreateDialogView()
	{
		String path = ResourcesManager.<String>getSetting("DefaultPath");
		tab = new TabView(path, true);
		
		return tab;
	}
	
	/**
	 * Called when dialog is closed, saves setting on positive result.
	 * 
	 * @param positiveResult True if user clicked ok.
	 */
	@Override
	protected void onDialogClosed(boolean positiveResult)
	{
		if (positiveResult)
		{
			ResourcesManager.setSetting("DefaultPath", tab.getCurrentPath());
		}
		
		TabManager.fixSingleTabBug();
	}
}
