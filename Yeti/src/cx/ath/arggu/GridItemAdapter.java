/*
*  GridItemAdapter.java
*
*  Copyright information
*
*      Copyright (C) 2011-2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import java.util.ArrayList;
import java.util.Iterator;
import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/** 
 * Class for folder items GridView Adapter.
 *
 * @author Arttu Liimola
 * @version 1.1
 * date: 15.01.2012
 */
public class GridItemAdapter extends BaseAdapter
{
	/** Interface to global information about an application environment. */
	private Context context;
	
	/** Folder items. */
	private ArrayList<Item> items;
	
	/**	Id of selected drawable. */
	private int selectedDrawable;
	
	/**	Id of not selected drawable. */
	private int notSelectedDrawable;
	
	/**
	 * Constructor.
	 * 
	 * @param context Interface to global information about an application environment.
	 * @param items   Folder items.
	 */
	public GridItemAdapter(Context context, ArrayList<Item> items)
    {
        this.context             = context;
        this.items               = items;
        this.selectedDrawable    = R.drawable.folder_view_selected;
        this.notSelectedDrawable = R.drawable.folder_view_not_selected;
    }

	/**
	 * Gets count of items.
	 * 
	 * @return Count of items.
	 */
	@Override
	public int getCount()
	{
		return items.size();
	}

	/**
	 * Gets item.
	 * 
	 * @param position Position for the item.
	 * 
	 * @return Returns item.
	 */
	@Override
	public Object getItem(int position)
	{
		return items.get(position);
	}

	/**
	 * Gets item's id.
	 * 
	 * @param position Position for the item.
	 * 
	 * @return Returns item's id.
	 */
	@Override
	public long getItemId(int position)
	{
		return position;
	}
	
	/**
	 * Get view for item.
	 * 
	 * @param position    Position for the item.
	 * @param convertView Recycled View instance.
	 * @param parent      View's parent.
	 * 
	 * @return Returns view.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view = convertView;
		Item item = items.get(position);
		
		ImageView icon;
		TextView name;
		
		if (view == null)
		{
			LayoutInflater li = LayoutInflater.from(context);
            view = li.inflate(R.layout.grid_item, null);
		}

        icon = (ImageView) view.findViewById(R.id.icon);
        icon.getLayoutParams().width  = ResourcesManager.<Integer>getSetting("IconSize");
        icon.getLayoutParams().height = ResourcesManager.<Integer>getSetting("IconSize");
        icon.setImageBitmap(item.getIcon());
        
        int filenameSize = ResourcesManager.<Integer>getSetting("FilenameSize");
        name             = (TextView) view.findViewById(R.id.name);
		name.setText(item.getName());
		name.setTextSize(TypedValue.COMPLEX_UNIT_SP, filenameSize);
		
		view.setBackgroundResource(item.isSelected() ? selectedDrawable : notSelectedDrawable);

		return view;
	}

	/**
	 * Get items.
	 * 
	 * @return Items.
	 */
	public ArrayList<Item> getItems()
	{
		return items;
	}
	
	/**
	 * Get item by name.
	 * 
	 * @param name Item name.
	 * 
	 * @return Item.
	 */
	public Item getItemByName(String name)
	{
		Iterator<Item> itemsIt = items.iterator();
		Item cItem             = null;
		Item item              = null;
		
		while(itemsIt.hasNext())
		{
			cItem = itemsIt.next();
			
			if (cItem.getName().equals(name))
				item = cItem;
		}
		
		return item;
	}
}
