/*
*  IconSizePreference.java
*
*  Copyright information
*
*      Copyright (C) 2011-2015 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import android.content.Context;
import android.graphics.Bitmap;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

/** 
 * Class for icon size dialog preference.
 *
 * @author Arttu Liimola
 * @version 1.2
 * date: 04.02.2015
 */
public class IconSizePreference extends DialogPreference implements OnSeekBarChangeListener
{
	/** Interface to global information about an application environment. */
	private Context context;
	
	/** View that show value to user. */
	private Toast toast;
	
	/** ImageView that previews icon size. */
	private ImageView iconView;
	
	/** Seekbar that is used to change icon size. */
	private SeekBar seekBar;
	
	/**
	 * Constructor.
	 * 
	 * @param context Interface to global information about an application environment.
	 * @param attrs   Collection of attributes from XML-file.
	 */
	public IconSizePreference(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		
		this.context = context;
		this.toast   = null;
	}

	/**
	 * Called when seekbar value is changed.
	 * 
	 * @param seekbar  Seekbar thats values is changed.
	 * @param progress Seekbar's value.
	 * @param fromUser Specifies if the value was changed by user.
	 */
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
	{
		int iconSize = (progress + 2) * 16;
		String msg   = "Size: " + Integer.toString(iconSize) + "px";	
		Bitmap icon  = ResourcesManager.getIcon("places/folder.png", iconSize);
		
		if (icon != null)
		{
			int padding = (192 - iconSize) / 2;
			iconView.setImageBitmap(icon);
			iconView.setPadding(0, padding, 0,padding);
		}
		
		if (toast != null)
			toast.setText(msg);
		else
			toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);

		toast.show();
	}

	/**
	 * Called when user starts touch gesture on seekbar.
	 * 
	 * @param seekbar Seekbar in which the touch gesture began.
	 */
	@Override
	public void onStartTrackingTouch(SeekBar seekBar)
	{
	}

	/**
	 * Called when user finishes touch gesture on seekbar.
	 * 
	 * @param seekbar Seekbar in which the touch gesture finishes.
	 */
	@Override
	public void onStopTrackingTouch(SeekBar seekBar)
	{
	}
	
	/**
	 * Creates content view for dialog.
	 * 
	 * @return Content view for dialog.
	 */
	@Override
	protected View onCreateDialogView()
	{
		LayoutInflater li  = LayoutInflater.from(context);
		View layout        = li.inflate(R.layout.settings_icon_size, null);
		iconView           = (ImageView)layout.findViewById(R.id.settingsIconSizeImage);
		seekBar            = (SeekBar)layout.findViewById(R.id.settingsIconSizeSlider);
		int iconSize       = ResourcesManager.<Integer>getSetting("IconSize");
		Bitmap icon        = ResourcesManager.getIcon("places/folder.png", iconSize);
		
		if (icon != null)
		{
			int padding = (192 - iconSize) / 2;
			iconView.setImageBitmap(icon);
			iconView.setPadding(0, padding, 0,padding);
		}

		seekBar.setMax(10);
		seekBar.setProgress((iconSize / 16) - 2);
		seekBar.setOnSeekBarChangeListener(this);
		
		return layout;
	}
	
	/**
	 * Called when dialog is closed, saves setting on positive result.
	 * 
	 * @param positiveResult True if user clicked ok.
	 */
	@Override
	protected void onDialogClosed(boolean positiveResult)
	{
		if (positiveResult)
		{
			int iconSize = (seekBar.getProgress() + 2) * 16;

			if (iconSize != ResourcesManager.<Integer>getSetting("IconSize"))
			{
				ResourcesManager.setSetting("IconSize", iconSize);
				
				ResourcesManager.reloadIcons();
				FolderManager.reloadIcons();
			}
		}
			
	}
}
