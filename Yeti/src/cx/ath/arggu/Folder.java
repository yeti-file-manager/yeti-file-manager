/*
*  Folder.java
*
*  Copyright information
*
*      Copyright (C) 2011-2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;
import org.json.JSONArray;
import org.json.JSONException;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.webkit.MimeTypeMap;

/** 
 * Class for folder.
 *
 * @author Arttu Liimola
 * @version 1.5
 * date: 16.01.2012
 */
public class Folder
{
	/** Interface to global information about an application environment. */
	private static Context context;
	
	/** Views for items in folder. */
	private HashMap<TabView, FolderView> folderViews;
	
	/** Folder's path. */
	private String path;
	
	/** Folder's name. */
	private String name;
	
	/** How files are sorted. */
	private String sortBy;
	
	/** Folder's folders. */
	private HashMap<String, TreeMap<?, ?>> folders;
	
	/** Folder's files. */
	private HashMap<String, TreeMap<?, ?>> files;
	
	/** Folder's items sorted. */
	private ArrayList<Item> sortedItems;
	
	/** Is folder readable. */
	private boolean canRead;
	
	/** Is folder writable. */
	private boolean canWrite;
	
	/** Show hidden. */
	private boolean showHidden;
	
	/** Preview images and videos */
	private boolean showPreview;

	/**	Is folder remote. */
	private boolean isRemote;
	
	/**
	 * Initializes static variables.
	 * 
	 * @param context Interface to global information about an application environment.
	 */
	public static void set(Context context)
	{
		Folder.context = context;
	}
	
	/**
	 * Constructor.
	 * 
	 * @param path        Folder path.
	 * @param onlyFolders Show only folders.
	 */
	public Folder(String path, boolean onlyFolders)
	{
		this.path   = path;
		
		if (!path.equals("/"))
		{
			String[] names = path.split("/");
			this.name      = names[names.length - 1];
		}
		else
		{
			this.name = path;
		}
		
		folderViews = new HashMap<TabView, FolderView>();
		folders     = new HashMap<String, TreeMap<?, ?>>();
		files       = new HashMap<String, TreeMap<?, ?>>();
		sortedItems = new ArrayList<Item>();
		isRemote    = false;
		
		readFolder(onlyFolders);
	}

	/**
	 * Refresh folder.
	 */
	public void refresh()
	{
		readFolder(false);
		
		Iterator<FolderView> folderViewsIt = folderViews.values().iterator();
		
		while (folderViewsIt.hasNext())
			folderViewsIt.next().setAdapter(new GridItemAdapter(context, sortedItems));
	}
	
	/**
	 * Reloads icons.
	 */
	public void reloadIcons()
	{
		ArrayList<Item> items = new ArrayList<Item>();
		items.addAll(((TreeMap<String, Item>)folders.get("name")).values());
		items.addAll(((TreeMap<String, Item>)files.get("name")).values());
		
		Iterator<Item> itemsIt = items.iterator();
		Item item;
		File file;
		
		while (itemsIt.hasNext())
		{
			item        = itemsIt.next();
			file        = new File(path + item.getName());
			String mime = item.getMime();
			
			item.setIcon(loadIconForFile(file, mime));
		}
		
		Iterator<FolderView> folderViewsIt = folderViews.values().iterator();
		FolderView folderView              = null;
		
		while (folderViewsIt.hasNext())
		{
			folderView = folderViewsIt.next();
			
			folderView.setColumnWidth(ResourcesManager.<Integer>getSetting("IconSize"));
			folderView.setAdapter(new GridItemAdapter(context, sortedItems));
		}
	}
	
	/**
	 * Resets filename size.
	 */
	public void resetFilenameSize()
	{
		Iterator<FolderView> folderViewsIt = folderViews.values().iterator();
		
		while (folderViewsIt.hasNext())
			folderViewsIt.next().setAdapter(new GridItemAdapter(context, sortedItems));
	}
	
	/**
	 * Get view.
	 * 
	 * @param tab Tab that needs the view.
	 * 
	 * @return Returns view for items in the folder.
	 */
	public FolderView getView(TabView tab)
	{
		FolderView folderView = folderViews.get(tab);
		
		if (folderView == null)
		{
			LayoutInflater li = LayoutInflater.from(context);
			folderView        = (FolderView)li.inflate(R.layout.icon_view, null);
			
			folderView.setColumnWidth(ResourcesManager.<Integer>getSetting("IconSize"));
			folderView.setAdapter(new GridItemAdapter(context, sortedItems));
			
			folderViews.put(tab, folderView);
		}
		
		return folderView;
	}
	
	/**
	 * Get path.
	 * 
	 * @return Returns folder's path.
	 */
	public String getPath()
	{
		return path;
	}
	
	/**
	 * Get name.
	 * 
	 * @return Returns folder's name.
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Get sorting.
	 * 
	 * @return Returns how folder's items are sorted.
	 */
	public String getSorting()
	{
		return sortBy;
	}
	
	/**
	 * Get item from folder.
	 * 
	 * @param name Item's name.
	 * 
	 * @return Returns item.
	 */
	public Item getItem(String name)
	{
		Item item = null;
		Item cItem;
		
		Iterator<Item> itemsIt = sortedItems.iterator();
		
		while(itemsIt.hasNext())
		{
			cItem = itemsIt.next();
			
			if (cItem.getName().equals(name))
				item = cItem;
		}
		
		return item;
	}
	
	/**
	 * Sets sorting.
	 * 
	 * @param sortBy Specifies how items are sorted.
	 */
	public void setSorting(String sortBy)
	{	
		this.sortBy = sortBy;
		
		generateSortedItems();
		
		Iterator<FolderView> folderViewsIt = folderViews.values().iterator();
		
		while (folderViewsIt.hasNext())
			folderViewsIt.next().setAdapter(new GridItemAdapter(context, sortedItems));
	}
	
	/**
	 * Sets show hidden.
	 * 
	 * @param showHidden Specifies if hidden items are shown.
	 */
	public void setShowHidden(boolean showHidden)
	{
		this.showHidden = showHidden;
		
		generateSortedItems();
		
		Iterator<FolderView> folderViewsIt = folderViews.values().iterator();
		
		while (folderViewsIt.hasNext())
			folderViewsIt.next().setAdapter(new GridItemAdapter(context, sortedItems));
	}
	
	/**
	 * Sets show previews.
	 * 
	 * @param showPreview Specifies if previews are shown.
	 */
	public void setShowPreviews(boolean showPreview)
	{
		this.showPreview = showPreview;
		
		Iterator<Item> itemsIt = sortedItems.iterator();
		Item item;
		File file;
		
		while (itemsIt.hasNext())
		{
			item        = itemsIt.next();
			file        = new File(path + item.getName());
			String mime = item.getMime();
			
			item.setIcon(loadIconForFile(file, mime));
		}
		
		Iterator<FolderView> folderViewsIt = folderViews.values().iterator();
		
		while (folderViewsIt.hasNext())
			folderViewsIt.next().invalidateViews();
	}
	
	/**
	 * Checks if folder is readable.
	 * 
	 * @return Returns true if folder is readable.
	 */
	public boolean canRead()
	{
		return canRead;
	}
	
	/**
	 * Checks if folder is writable.
	 * 
	 * @return Returns true if folder is writable.
	 */
	public boolean canWrite()
	{
		return canWrite;
	}
	
	/**
	 * Checks if folder is remote.
	 * 
	 * @return Returns true if folder is remote.
	 */
	public boolean isRemote()
	{
		return isRemote;
	}
	
	/**
	 * Checks if item exists in the folder.
	 * 
	 * @return Returns true if items exists.
	 */
	public boolean itemExists(String name)
	{
		boolean exists = false;
		
		if (!isRemote)
		{
			File file = new File(path + name);
			exists    = file.exists();
		}
		
		return exists;
	}
	
	/**
	 * Checks if folder shows hidden items.
	 * 
	 * @return Return true if folder shows hidden items.
	 */
	public boolean showsHidden()
	{
		return showHidden;
	}
	
	/**
	 * Checks if folder shows previews.
	 * 
	 * @return Return true if folder shows previews.
	 */
	public boolean showsPreviews()
	{
		return showPreview;
	}
	
	/**
	 * Removes view.
	 * 
	 * @param tab Tab that is the view's parent.
	 * 
	 * @return Returns removed view.
	 */
	public FolderView removeView(TabView tab)
	{
		return folderViews.remove(tab);
	}
	
	/**
	 * Reads folder.
	 * 
	 * @param onlyFolders If true only folders are showed.
	 */
	private void readFolder(boolean onlyFolders)
	{	
		folders.put("name", new TreeMap<String, Item>(String.CASE_INSENSITIVE_ORDER));
		folders.put("date", new TreeMap<Long, Item>());
		files.put("name", new TreeMap<String, Item>(String.CASE_INSENSITIVE_ORDER));
		files.put("date", new TreeMap<Long, Item>());
		files.put("type", new TreeMap<String, Item>(String.CASE_INSENSITIVE_ORDER));
		
		TreeMap<String, Item> foldersName = (TreeMap<String, Item>)folders.get("name");
		TreeMap<Long, Item> foldersDate   = (TreeMap<Long, Item>)folders.get("date");
		TreeMap<String, Item> filesName   = (TreeMap<String, Item>)files.get("name");
		TreeMap<Long, Item> filesDate     = (TreeMap<Long, Item>)files.get("date");
		TreeMap<String, Item> filesType   = (TreeMap<String, Item>)files.get("type");
		
		File folder  = new File(path);
		File[] items = folder.listFiles();
		canRead      = folder.canRead();
		canWrite     = folder.canWrite();
		sortBy       = FolderManager.getSorting(path);
		showHidden   = FolderManager.showsHidden(path);
		showPreview  = FolderManager.showsPreviews(path);
		
		if (items != null && items.length > 0)
		{
			MimeTypeMap map = MimeTypeMap.getSingleton();
			
			File item;
			String name;
			String mime;
			String ext;
			Bitmap icon;
			long date;

			for (int i = 0; i < items.length; i++)
			{
				mime = "";
				item = items[i];
				name = item.getName();

				if (item.isDirectory())
				{
					mime = "folder";
					icon = ResourcesManager.loadIcon(item, mime);
					
					Item newItem = new Item(name, mime, icon, item.isHidden());
					foldersName.put(name, newItem);

					for (date = item.lastModified(); foldersDate.containsKey(date); date++);
					
					foldersDate.put(date, newItem);
				}
				else if (!onlyFolders)
				{
					ext = MimeTypeMap.getFileExtensionFromUrl(name);
					
					if (ext.length() == 0)
					{
						int dot = name.lastIndexOf('.');
						
						if (dot != -1)
						{
							ext = name.substring(dot + 1);
						}
					}
					
					mime         = map.getMimeTypeFromExtension(ext);
					mime         = mime != null ? mime : "unknown";
					icon         = loadIconForFile(item, mime);
					Item newItem = new Item(name, mime, icon, item.isHidden());
					filesName.put(name, newItem);
					
					for (date = item.lastModified(); filesDate.containsKey(date); date++);
					
					filesDate.put(date, newItem);
					filesType.put(ext + name, newItem);
				}
			}
		}
		
		generateSortedItems();
	}
	
	/**
	 * Generates sorted items ArrayList.
	 */
	private void generateSortedItems()
	{
		TreeMap<String, Item> foldersName = (TreeMap<String, Item>)folders.get("name");
		TreeMap<Long, Item> foldersDate   = (TreeMap<Long, Item>)folders.get("date");
		TreeMap<String, Item> filesName   = (TreeMap<String, Item>)files.get("name");
		TreeMap<Long, Item> filesDate     = (TreeMap<Long, Item>)files.get("date");
		TreeMap<String, Item> filesType   = (TreeMap<String, Item>)files.get("type");
		
		Iterator<Item> itemsIt;
		Item cItem;
		
		sortedItems.clear();
		
		if (sortBy.equals("name"))
		{
			if (!showHidden)
			{
				itemsIt = foldersName.values().iterator();
				
				while (itemsIt.hasNext())
				{
					cItem = itemsIt.next();
					
					if (!cItem.isHidden())
						sortedItems.add(cItem);
				}
				
				itemsIt = filesName.values().iterator();
				
				while (itemsIt.hasNext())
				{
					cItem = itemsIt.next();
					
					if (!cItem.isHidden())
						sortedItems.add(cItem);
				}
			}
			else
			{
				sortedItems.addAll(foldersName.values());
				sortedItems.addAll(filesName.values());
			}
		}
		else if(sortBy.equals("date"))
		{
			if (!showHidden)
			{
				itemsIt = foldersDate.values().iterator();
				
				while (itemsIt.hasNext())
				{
					cItem = itemsIt.next();
					
					if (!cItem.isHidden())
						sortedItems.add(cItem);
				}
				
				itemsIt = filesDate.values().iterator();
				
				while (itemsIt.hasNext())
				{
					cItem = itemsIt.next();
					
					if (!cItem.isHidden())
						sortedItems.add(cItem);
				}
			}
			else
			{
				sortedItems.addAll(foldersDate.values());
				sortedItems.addAll(filesDate.values());
			}
		}
		else
		{
			if (!showHidden)
			{
				itemsIt = foldersName.values().iterator();
				
				while (itemsIt.hasNext())
				{
					cItem = itemsIt.next();
					
					if (!cItem.isHidden())
						sortedItems.add(cItem);
				}
				
				itemsIt = filesType.values().iterator();
				
				while (itemsIt.hasNext())
				{
					cItem = itemsIt.next();
					
					if (!cItem.isHidden())
						sortedItems.add(cItem);
				}
			}
			else
			{
				sortedItems.addAll(foldersName.values());
				sortedItems.addAll(filesType.values());
			}
		}
	}
	
	/**
	 * Load icon for file.
	 * 
	 * @param item File.
	 * @param mime File's MIME type.
	 * 
	 * @return Returns icon for file.
	 */
	private Bitmap loadIconForFile(File item, String mime)
	{
		Bitmap icon;
		
		if ((mime.contains("image") || mime.contains("video")) && showPreview)
		{
			icon = ResourcesManager.loadPreview(item, mime);
			
			if (icon == null)
				icon = ResourcesManager.loadIcon(item, mime);
		}
		else
		{
			icon = ResourcesManager.loadIcon(item, mime);
		}
		
		return icon;
	}
}
