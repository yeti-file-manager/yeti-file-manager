/*
*  TabManager.java
*
*  Copyright information
*
*      Copyright (C) 2011-2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;
import android.widget.TabHost;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabHost.TabSpec;

/** 
 * Class for tab manager.
 *
 * @author Arttu Liimola
 * @version 1.2
 * date: 05.01.2012
 */
public class TabManager implements OnTabChangeListener
{
	/** Tabs. */
	private static ArrayList<TabView> tabs;
	
	/** Current tab */
	private static int currentTab;
	
	/** Self. */
	private static TabManager self;
	
	/** Container for tabbed window view. */
	private static TabHost tabHost;
	
	/** Odd bug when opening tab after accessing default folder setting with single tab open. */
	private static boolean fixSingleTabBug;
	
	/**
	 *	Initializes variables of this class.
	 * 
	 * @param tabHost    Container for tabbed window view.
	 * @param tabs       Tabs that are saved on onSaveInstanceState.
	 * @param currentTab Current tab index saved on onSaveInstanceState.
	 */
	public static void set(TabHost tabHost, ArrayList<HashMap<String, Boolean>> tabs, int currentTab)
	{
		TabManager.tabs            = new ArrayList<TabView>();
		TabManager.self            = new TabManager();
		TabManager.tabHost         = tabHost;
		TabManager.fixSingleTabBug = false;
		TabManager.tabHost.setOnTabChangedListener(self);
		
		if (tabs != null && currentTab != -1)
		{
			Iterator<HashMap<String, Boolean>> tabsIt = tabs.iterator();
			
			while (tabsIt.hasNext())
			{
				createTab(tabsIt.next());
			}
			
			TabManager.currentTab = currentTab;
	    	tabHost.setCurrentTab(currentTab);
	    	
	    	if (tabs.size() == 1)
	    		tabHost.getTabWidget().setVisibility(View.GONE);
		}
		else
		{
			createTab(ResourcesManager.<String>getSetting("DefaultPath"));
			tabHost.getTabWidget().setVisibility(View.GONE);
		}
	}
	
	/**
	 * Called when tab is changed.
	 * 
	 * @param tabId Tab's id.
	 */
	@Override
	public void onTabChanged(String tabId)
	{
		currentTab = Integer.parseInt(tabId);
	}
	
	/**
	 * Creates tab.
	 * 
	 * @param path Path to the folder in tab.
	 */
	public static void createTab(String path)
	{
		TabView tab;
		TabSpec spec;
		
		if (fixSingleTabBug)
		{
			tab  = tabs.get(0);
			spec = tabHost.newTabSpec("0");
			
			spec.setContent(new CreateTabContent(tab));
	    	spec.setIndicator(tab.getHeader());
			tabHost.clearAllTabs();
	    	tabHost.addTab(spec);
	    	
	    	fixSingleTabBug = false;
		}
		
		if (tabs.size() == 1)
	    	tabHost.getTabWidget().setVisibility(View.VISIBLE);
		
		tab  = new TabView(path);
    	spec = tabHost.newTabSpec(Integer.toString(tabs.size()));
    	spec.setContent(new CreateTabContent(tab));
    	spec.setIndicator(tab.getHeader());
    	tabHost.addTab(spec);
    	tabs.add(tab);
    	
    	currentTab = tabs.size() -1;
    	tabHost.setCurrentTab(currentTab);
	}
	
	/**
	 * Creates tab.
	 * 
	 * @param folders Folders for tab.
	 */
	public static void createTab(HashMap<String, Boolean> folders)
	{
		TabView tab;
		TabSpec spec;
		
		if (tabs.size() == 1)
	    	tabHost.getTabWidget().setVisibility(View.VISIBLE);
		
		tab  = new TabView(folders);
    	spec = tabHost.newTabSpec(Integer.toString(tabs.size()));
    	spec.setContent(new CreateTabContent(tab));
    	spec.setIndicator(tab.getHeader());
    	tabHost.addTab(spec);
    	tabs.add(tab);
	}
	
	/**
	 * Closes current tab.
	 */
	public static void closeTab()
	{
		if (tabs.size() > 1)
		{
			tabs.remove(currentTab);
			tabHost.setCurrentTab(0);
			tabHost.clearAllTabs();
			
			Iterator<TabView> tabsIt = tabs.iterator();
			TabView tab;
			TabSpec spec;
			
			for (int i = 0; tabsIt.hasNext(); i++)
			{
				tab  = tabsIt.next();
				spec = tabHost.newTabSpec(Integer.toString(i));
		    	spec.setContent(new CreateTabContent(tab));
		    	spec.setIndicator(tab.getHeader());
		    	tabHost.addTab(spec);
			}
			
			currentTab = tabs.size() -1;
	    	tabHost.setCurrentTab(currentTab);
	    	
	    	if (currentTab == 0)
	    		tabHost.getTabWidget().setVisibility(View.GONE);
		}
	}
	
	/**
	 * Changes tab's header.
	 * 
	 * @param header Tab's header.
	 */
	public static void changeHeader(String header)
	{
    	ViewGroup tab = (ViewGroup)tabHost.getTabWidget().getChildAt(currentTab);
    	TextView text = (TextView)tab.getChildAt(1);
    	text.setText(header);
	}

	/**
	 * Get tabs.
	 * 
	 * @return Returns tabs.
	 */
	public static ArrayList<TabView> getTabs()
	{
		return tabs;
	}
	
	/**
	 * Get current tab.
	 * 
	 * @return Returns current tab.
	 */
	public static TabView getCurrentTab()
	{
		return tabs.get(currentTab);
	}
	
	/**
	 * Get current tab index.
	 * 
	 * @return Returns current tab index.
	 */
	public static int getCurrentTabIndex()
	{
		return currentTab;
	}
	
	/**
	 * For fixing a bug.
	 * 
	 * Odd bug when opening tab after accessing default folder setting with single tab open.
	 */
	public static void fixSingleTabBug()
	{
		if (tabs.size() == 1)
		{
			fixSingleTabBug = true;
		}
	}
	
	/** 
	 * Class for creating tab content.
	 *
	 * @author Arttu Liimola
	 * @version 1.0
	 * date: 11.12.2011
	 */
	private static class CreateTabContent implements TabContentFactory
    {
		/** View for tab. */
    	private final View tab;

    	/**
    	 * Constructor.
    	 * 
    	 * @param view View for tab.
    	 */
    	protected CreateTabContent(View view)
    	{
    		tab = view;
    	}

    	/**
    	 * Creates tab contents.
    	 * 
    	 * @param tag Selected tab's tag.
    	 * 
    	 * @return Returns view for tab.
    	 */
    	public View createTabContent(String tag) 
    	{
    		return tab;
    	}
	}
}
