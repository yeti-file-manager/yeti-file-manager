/*
*  Item.java
*
*  Copyright information
*
*      Copyright (C) 2011-2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import android.graphics.Bitmap;

/** 
 * Class for item.
 *
 * @author Arttu Liimola
 * @version 1.2
 * date: 04.01.2012
 */
public class Item
{
	/** Item's name. */
	private String name;
	
	/** Item's MIME type. */
	private String mime;
	
	/** Item's icon. */
	private Bitmap icon;
	
	/** Is item hidden. */
	private boolean hidden;
	
	/** Is item selected. */
	private boolean selected;
	
	/**
	 * Constructor.
	 * 
	 * @param name   Item's name.
	 * @param mime   Item's MIME type.
	 * @param icon   Item's icon.
	 * @param hidden Specifies if item is hidden.
	 */
	public Item(String name, String mime, Bitmap icon, boolean hidden)
	{
		this.name     = name;
		this.mime     = mime;
		this.icon     = icon;
		this.hidden   = hidden;
		this.selected = false;
	}
	
	/**
	 * Get item's name.
	 * 
	 * @return Returns item name.
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Get item's MIME type.
	 * 
	 * @return Returns item MIME type.
	 */
	public String getMime()
	{
		return mime;
	}
	
	/**
	 * Get item's icon.
	 * 
	 * @return Returns item icon.
	 */
	public Bitmap getIcon()
	{
		return icon;
	}
	
	/**
	 * Set item's icon.
	 * 
	 * @param icon Icon.
	 */
	public void setIcon(Bitmap icon)
	{
		this.icon = icon;
	}
	
	/**
	 * Sets item's selection.
	 * 
	 * @param selected True if item is selected.
	 */
	public void setSelected(boolean selected)
	{
		this.selected = selected;
	}
	
	/**
	 * Checks if item is hidden.
	 * 
	 * @return Returns true if item is hidden.
	 */
	public boolean isHidden()
	{
		return hidden;
	}
	
	/**
	 * Checks if item is selected.
	 * 
	 * @return Returns true if item is selected.
	 */
	public boolean isSelected()
	{
		return selected;
	}
}
