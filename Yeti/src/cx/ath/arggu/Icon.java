/*
*  Icon.java
*
*  Copyright information
*
*      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import java.io.File;
import java.io.IOException;
import java.util.List;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;

/**
 * Class for icon.
 *
 * @author Arttu Liimola
 * @version 1.1
 * date: 28.12.2011
 */
public class Icon
{
	/** Icon. */
	private Bitmap icon;
	
	/** Icon name. */
	private String iconName;
	
	/** Is icon loaded via intent */
	private boolean loadedViaIntent;
	
	/**
	 * Constructor.
	 * 
	 * @param file   File that needs icon.
	 * @param type   MIME type.
	 * @param assets Application assets.
	 * @param pm     Package manager.
	 */
	public Icon(File file, String type, AssetManager assets, PackageManager pm)
	{
		String size     = Integer.toString(ResourcesManager.<Integer>getSetting("IconSize"));
		String folder   = "icons/oxygen/" + size + "x" + size + "/";
		String unknown  = "mimetypes/unknown.png";
		String icon     = "";
		String generic  = "";
		boolean ok      = false;
		loadedViaIntent = false;
		
		if (type != null && type.contains("/"))
		{
			String[] mime = type.split("/");
			icon          = "mimetypes/" + mime[0] + "-" + mime[1] + ".png";
			generic       = "mimetypes/" + mime[0] + "-x-generic.png";
		}
		else if (type.equals("folder"))
		{
			icon = "places/" + type + ".png";
		}
		else
		{
			icon = unknown;
		}
		
		try
		{
			if (ResourcesManager.isIconAvailabe(icon))
			{
				iconName  = icon;
				this.icon = BitmapFactory.decodeStream(assets.open(folder + icon));
				
				ok = true;
			}
			else if(ResourcesManager.isIconAvailabe(generic))
			{
				iconName  = generic;
				this.icon = BitmapFactory.decodeStream(assets.open(folder + generic));
				
				ok = true;
			}
			else
			{
				ok = loadIconViaIntent(file, type, pm);
				
				loadedViaIntent = ok;
			}
		}
		catch (IOException e)
		{
		}
		
		if (!ok)
		{
			try
			{	
				iconName  = unknown;
				this.icon = BitmapFactory.decodeStream(assets.open(folder + unknown));
			}
			catch (IOException e)
			{
			}
		}
	}
	
	/**
	 * Reloads icon that was not loaded via intent.
	 * 
	 * @param assets Application assets.
	 */
	public void reloadIcon(AssetManager assets)
	{
		if (!loadedViaIntent)
		{
			String size = Integer.toString(ResourcesManager.<Integer>getSetting("IconSize"));
			String file = "icons/oxygen/" + size + "x" + size + "/" + iconName;
			
			try
			{	
				icon = BitmapFactory.decodeStream(assets.open(file));
			}
			catch (IOException e)
			{
			}
		}
	}
	
	/**
	 * Get icon.
	 * 
	 * @return Returns icon.
	 */
	public Bitmap getIcon()
	{
		return icon;
	}
	
	/**
	 * Load icon via intent.
	 * 
	 * @param file File that needs icon.
	 * @param type MIME type.
	 * @param pm   Package manager.
	 * 
	 * @return Returns true if icon was loaded.
	 */
	private boolean loadIconViaIntent(File file, String type, PackageManager pm)
	{
		boolean ok = false;
		
		Uri data = Uri.fromFile(file);

		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(data, type);
	     
	   	List<ResolveInfo> activities = pm.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
	   	 
	   	if (activities.size() > 0)
	   	{ 		 
	   		ResolveInfo activity = activities.get(activities.size() - 1);
	   		icon = ((BitmapDrawable)activity.loadIcon(pm)).getBitmap();
	   		
	   		ok = true;
	   	}
	   	
	   	return ok;
	}
}
