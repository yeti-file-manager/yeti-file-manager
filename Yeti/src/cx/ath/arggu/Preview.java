/*
*  Preview.java
*
*  Copyright information
*
*      Copyright (C) 2011 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;

/** 
 * Class for preview.
 *
 * @author Arttu Liimola
 * @version 1.0
 * date: 30.12.2011
 */
public class Preview
{
	/** Preview. */
	private Bitmap preview;
	
	/** File's path. */
	private String path;
	
	/** File's MIME type. */
	private String mime;
	
	/**
	 * Constructor.
	 * 
	 * @param file File.
	 * @param mime File's MIME type.
	 * @param cr   Provides applications access to the content model.
	 */
	public Preview(File file, String mime, ContentResolver cr)
	{
		preview   = null;
		path      = file.getAbsolutePath();
		this.mime = mime;
		
		loadPreview(cr);
	}
	
	/**
	 * Get preview.
	 * 
	 * @return Returns preview.
	 */
	public Bitmap getPreview()
	{
		return preview;
	}
	
	/**
	 * Reloads preview.
	 * 
	 * @param cr Provides applications access to the content model.
	 */
	public void reloadPreview(ContentResolver cr)
	{
		loadPreview(cr);
	}
	
	/**
	 * Load preview.
	 * 
	 * @param cr Provides applications access to the content model.
	 */
	private void loadPreview(ContentResolver cr)
	{
		Uri uri;
		String data;
		String[] projection = new String[1];
		Cursor cursor;
		int kind;
		
		if (mime.contains("image"))
		{
			uri           = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
			data          = MediaStore.Images.Media.DATA;        
			projection[0] = MediaStore.Images.Media._ID;
			kind          = MediaStore.Images.Thumbnails.MINI_KIND;
		}
		else
		{
			uri           = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
			data          = MediaStore.Video.Media.DATA;
			projection[0] = MediaStore.Video.Media._ID;
			kind          = MediaStore.Video.Thumbnails.MINI_KIND;
		}
		
		cursor = cr.query(uri, projection, data + " LIKE '/mnt" + path + "'", null, null);
		
		if (cursor == null || cursor.getCount() < 1)
			cursor = cr.query(uri, projection, data + " LIKE '" + path + "'", null, null);
		
		if (cursor != null && cursor.getCount() > 0)
		{
			cursor.moveToFirst();
			
			int id = cursor.getInt(0);

			cursor.close();
			
			if (mime.contains("image"))
				preview = MediaStore.Images.Thumbnails.getThumbnail(cr, id, kind, null);
			else
				preview = MediaStore.Video.Thumbnails.getThumbnail(cr, id, kind, null);
		}
		
		if (preview == null)
		{
			if (mime.contains("image"))
			{
				try
				{
					FileInputStream is = new FileInputStream(new File(path));
					preview            = BitmapFactory.decodeStream(is);
				}
				catch (FileNotFoundException e)
				{
				}
			}
			else
			{		
				preview = ThumbnailUtils.createVideoThumbnail(path, kind);
			}
		}
		
		if (preview != null)
		{
			int[] size = calculateSize(preview.getWidth(), preview.getHeight());
            preview    = Bitmap.createScaledBitmap(preview, size[0], size[1], false);
		}
	}
	
	/**
	 * Calculates preview new size.
	 * 
	 * @param width  Preview original width.
	 * @param height Preview original height.
	 * 
	 * @return Return Preview new size.
	 */
	private int[] calculateSize(int width, int height)
	{
		int maxSize  = ResourcesManager.<Integer>getSetting("IconSize");
		int[] size   = {0, 0};
		float ratio  = (float)width / (float)height;
		
		if ((maxSize / ratio) <= maxSize)
		{
			size[0] = maxSize;
			size[1] = Math.round(maxSize / ratio);
		}
		else
		{
			size[0] = Math.round(maxSize * ratio);
			size[1] = maxSize;
		}
		
		return size;
	}
}
