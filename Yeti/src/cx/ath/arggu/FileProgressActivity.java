/*
*  FileProgressActivity.java
*
*  Copyright information
*
*      Copyright (C) 2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

/** 
 * Class for file operation progress.
 *
 * @author Arttu Liimola
 * @version 1.2
 * date: 31.01.2012
 */
public class FileProgressActivity extends Activity implements OnClickListener
{
	
	/**	Instance of FileProgressActivity. */
	private static FileProgressActivity self;

	/**	TextView that has text from copying/moving items to where. */
	private static TextView fromToView;
	
	/**	TextView that name of the currently copying/moving file. */
	private static TextView fileView;
	
	/**	Progress bar. */
	private static ProgressBar progressBarView;
	
	/**	Cancel button. */
	private static Button cancel;
	
	/**	Text from copying/moving items to where. */
	private static String fromTo;
	
	/** Name of the currently copying/moving file.*/
	private static String file;
	
	/**	Progress. */
	private static int progress;
	
	/**
	 * Called when the activity is first created.
	 * 
	 * @param savedInstanceState If activity is re-initialized this contains data set in
     * onSaveInstanceState.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.file_progress_dialog);
	    setTitle("Copying files.");
	    
	    if (self != null)
	    	self.finish();
	    
	    self            = this;	    
	    fromToView      = (TextView)findViewById(R.id.fileProgressDialogFromTo);
	    fileView        = (TextView)findViewById(R.id.fileProgressDialogFile);
	    progressBarView = (ProgressBar)findViewById(R.id.fileProgressDialogBar);
	    cancel          = (Button)findViewById(R.id.fileProgressDialogCancel);
	    
	    fromToView.setText(fromTo);
	    fileView.setText(file);
	    progressBarView.setProgress(progress);
	    cancel.setOnClickListener(this);
	}

	/**
	 * Set data for dialog.
	 * 
	 * @param data Data for dialog.
	 */
	public static void setData(Bundle data)
	{
		if (fromToView != null)
		{
			int progress = data.getInt("progress");
			
			fromToView.setText(data.getString("fromTo"));
			fileView.setText(data.getString("file"));
			progressBarView.setProgress(progress);
			
			if (progress == 100)
				self.finish();
		}
		{
			fromTo   = data.getString("fromTo");
			file     = data.getString("file");
			progress = data.getInt("progress");
		}
	}

	/**
	 * Called when button is clicked.
	 * 
	 * @param v Button.
	 */
	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.fileProgressDialogCancel:
			{
				FileManagement.cancel();
			}
		}
	}
}
