/*
*  FileManagement.java
*
*  Copyright information
*
*      Copyright (C) 2011-2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/** 
 * Class for file management.
 *
 * @author Arttu Liimola
 * @version 1.4
 * date: 31.01.2012
 */
public class FileManagement implements OnClickListener, Runnable
{
	/** ENUM for actions. */
	private enum Actions
	{
		CutLocalToLocal,
		CopyLocalToLocal,
		DeleteFromLocal,
		rename,
		skip,
		replace,
		merge
	}
	
	/** Instance of FileManagement-class. */
	private static FileManagement self;
	
	/** Interface to global information about an application environment. */
	private static Context context;
	
	/** Class to notify user of events. */
	private static NotificationManager notificationManager;
	
	/** Create folder dialog. */
	private static Dialog createFolder;
	
	/** TextView that has destination of the new folder. */
	private static TextView createFolderInto;
	
	/** EditText that has the new folder's name. */
	private static EditText createFolderName;
	
	/** Create folder ok button. */
	private static Button createFolderOk;
	
	/** Create folder cancel button. */
	private static Button createFolderCancel;
	
	/** List of file operations. */
	private static ArrayList<FileOperation> fileOperations;
	
	/** Used to update ui from thread. */
	private static UpdateUi updateUi;
	
	/** Thread. */
	private static Thread thread;
	
	/** File operation progress notification. */
	private static Notification progressNotify;
	
	/** File operation queue notification. */
	private static Notification queueNotify;
	
	/** Dialog to asks from user what action to take when destination file exists. */
	private static Dialog fileExistDialog;
	
	/** Dialog to asks from user what action to take when destination folder exists. */
	private static Dialog folderExistDialog;
	
	/** Exists action. */
	private static Actions existsAction;
	
	/** File's or folder's new name. */
	private static String newName;
	
	/** It set true file operation will be canceled. */
	private static boolean cancel;
	
	/**
	 * Initializes variables of this class.
	 * 
	 * @param context Interface to global information about an application environment.
	 */
	public static void set(Context context)
	{
		FileManagement.self    = new FileManagement();
		FileManagement.context = context;
		
		String ns           = Context.NOTIFICATION_SERVICE;
		notificationManager = (NotificationManager)context.getSystemService(ns);
		
		createFolder = new Dialog(context);
		createFolder.setContentView(R.layout.create_folder);
		
		createFolderInto    = (TextView)createFolder.findViewById(R.id.createFolderInto);
		createFolderName    = (EditText)createFolder.findViewById(R.id.createFolderName);
		createFolderOk      = (Button)createFolder.findViewById(R.id.createFolderOk);
		createFolderCancel  = (Button)createFolder.findViewById(R.id.createFolderCancel);
		fileOperations      = new ArrayList<FileOperation>();
		updateUi            = self.new UpdateUi();
		thread              = null;
		progressNotify      = null;
		queueNotify         = null;
		cancel              = false;
		
		createFolderOk.setOnClickListener(self);
		createFolderCancel.setOnClickListener(self);
	}
	
	/**
	 * Create folder.
	 * 
	 * @param path Destination of new folder.
	 */
	public static void createFolder(String path)
	{
		createFolderInto.setText(path);
		createFolder.show();
	}
	
	/**
	 * Open file.
	 * 
	 * @param folder Where file is located.
	 * @param name   Name of file.
	 * @param mime   MIME type of file.
	 */
	public static void openFile(Folder folder, String name, String mime)
	{
		String path = folder.getPath() + name;
		
		if (mime != "unknown")
		{
			if (!folder.isRemote())
			{
				File file = new File(path);
				Uri data  = Uri.fromFile(file);
				
				Intent intent = new Intent(Intent.ACTION_VIEW);
			   	intent.setDataAndType(data, mime);
			   	
			   	try
			   	{
			   		context.startActivity(intent);
			   	}
			   	catch (ActivityNotFoundException e)
			   	{
			   		Toast.makeText(context, R.string.cannotOpenFile, Toast.LENGTH_SHORT).show();
			   	}
			}
		}
		else
		{
			Toast.makeText(context, R.string.cannotOpenFile, Toast.LENGTH_SHORT).show();
		}
	}
	
	/**
	 * Cut items.
	 * 
	 * @param from  Source of items to cut.
	 * @param to    Destination of items to cut.
	 * @param items Items to cut.
	 */
	public static void cut(Folder from, Folder to, ArrayList<String> items)
	{
		Actions action = null;

		if (!from.isRemote() && !to.isRemote())
		{
			action = Actions.CutLocalToLocal;
		}
		
		fileOperations.add(self.new FileOperation(from, to, new ArrayList<String>(items), action));
		fileOperationQueueNotify();
		
		if (thread == null)
		{
			thread = new Thread(self);
			thread.start();
		}
		else if (!thread.isAlive())
		{
			thread = new Thread(self);
			thread.start();
		}
	}
	
	/**
	 * Copy items.
	 * 
	 * @param from  Source of items to copy.
	 * @param to    Destination of items to copy.
	 * @param items Items to copy.
	 */
	public static void copy(Folder from, Folder to, ArrayList<String> items)
	{
		Actions action = null;

		if (!from.isRemote() && !to.isRemote())
		{
			action = Actions.CopyLocalToLocal;
		}
		
		fileOperations.add(self.new FileOperation(from, to, new ArrayList<String>(items), action));
		fileOperationQueueNotify();
		
		if (thread == null)
		{
			thread = new Thread(self);
			thread.start();
		}
		else if (!thread.isAlive())
		{
			thread = new Thread(self);
			thread.start();
		}
	}
	
	/**
	 * Delete items.
	 * 
	 * @param from  Source of items to delete.
	 * @param items Items to delete.
	 */
	public static void delete(Folder from, ArrayList<String> items)
	{
		Actions action = null;

		if (!from.isRemote())
		{
			action = Actions.DeleteFromLocal;
		}
		
		fileOperations.add(self.new FileOperation(from, null, new ArrayList<String>(items), action));
		fileOperationQueueNotify();
		
		if (thread == null)
		{
			thread = new Thread(self);
			thread.start();
		}
		else if (!thread.isAlive())
		{
			thread = new Thread(self);
			thread.start();
		}
	}
	
	/**
	 * Cancels current file operation.
	 */
	public static void cancel()
	{
		cancel = true;
	}
	
	/**
	 * Called when user clicks button from dialog.
	 * 
	 * @param v Button that was clicked.
	 */
	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.createFolderOk:
			{
				String name = createFolderName.getText().toString();
					
				if (name.length() > 0)
				{
					String path   = createFolderInto.getText().toString();
					Folder folder = FolderManager.getFolder(path);
					
					if (!folder.isRemote())
						createLocalFolder(path, name);
					else
						createRemoteFolder(folder, path, name);
				}
				else
				{
					Toast.makeText(context, R.string.folderNameEmpty, Toast.LENGTH_SHORT).show();
				}				
				
				break;
			}
			
			case R.id.createFolderCancel:
			{
				createFolderName.setText("");
				createFolder.dismiss();

				break;
			}
			
			case R.id.fileExistsRename:
			{
				EditText name = (EditText)fileExistDialog.findViewById(R.id.fileExistsNewName);
				existsAction  = Actions.rename;
				newName       = name.getText().toString();
				
				fileExistDialog.dismiss();
				
				break;
			}
			
			case R.id.fileExistsSkip:
			{
				existsAction = Actions.skip;
				
				fileExistDialog.dismiss();
				
				break;
			}
			
			case R.id.fileExistsReplace:
			{
				existsAction = Actions.replace;
				
				fileExistDialog.dismiss();
				
				break;
			}
			
			case R.id.folderExistsRename:
			{
				EditText name = (EditText)folderExistDialog.findViewById(R.id.folderExistsNewName);
				existsAction  = Actions.rename;
				newName       = name.getText().toString();
				
				folderExistDialog.dismiss();
				
				break;
			}
			
			case R.id.folderExistsSkip:
			{
				existsAction = Actions.skip;
				
				folderExistDialog.dismiss();
				
				break;
			}
			
			case R.id.folderExistsMerge:
			{
				existsAction = Actions.merge;
				
				folderExistDialog.dismiss();
				
				break;
			}
		}
		
	}
	
	/**
	 * Called when thread is started.
	 */
	@Override
	public void run()
	{
		FileOperation fileOperation = null;
		
		while (!fileOperations.isEmpty())
		{
			fileOperation = fileOperations.remove(0);

			Bundle data = new Bundle();
			Message msg = new Message();
			
			data.putBoolean("updateQueue", true);
			msg.setData(data);
			updateUi.sendMessage(msg);
			
			switch (fileOperation.getAction())
			{
				case CutLocalToLocal:
				{
					//cutFromLocalToLocal(fileOperation);
					
					break;
				}
			
				case CopyLocalToLocal:
				{
					copyFromLocalToLocal(fileOperation);
					
					break;
				}
				
				case DeleteFromLocal:
				{
					deleteFromLocal(fileOperation);
					
					break;
				}
			}
		}
	}
	
	/**
	 * Creates local folder.
	 * 
	 * @param path Destination where folder is going to be created.
	 * @param name Name of the folder.
	 */
	private static void createLocalFolder(String path, String name)
	{
		File dir = new File(path + name);
		
		if (!dir.exists())
		{
			createFolderName.setText("");
			createFolder.dismiss();	
			
			if (dir.mkdir())
				Toast.makeText(context, R.string.folderCreated, Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(context, R.string.cannotCreateFolder, Toast.LENGTH_SHORT).show();
		}
		else
		{
			Toast.makeText(context, R.string.folderAlreadyExists, Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Create remove folder.
	 * 
	 * @param folder Folder where new folder is going to be created.
	 * @param path   Destination where folder is going to be created.
	 * @param name   Name of the folder.
	 */
	private static void createRemoteFolder(Folder folder, String path, String name)
	{
		
	}
	
	/**
	 * Copy items from local folder to local folder.
	 * 
	 * @param fileOperation Instance of FileOperation.
	 */
	private static void copyFromLocalToLocal(FileOperation fileOperation)
	{
		File from                 = new File(fileOperation.getFrom().getPath());
		File to                   = new File(fileOperation.getTo().getPath());
		File fromItem             = null;
		File toItem               = null;
		String toPath             = to.getAbsolutePath() + "/";
		ArrayList<String> items   = fileOperation.getItems();
		ArrayList<File> fromItems = new ArrayList<File>();
		ArrayList<File> toItems   = new ArrayList<File>();
		FileInputStream in        = null;
		FileOutputStream out      = null;
		byte[] buffer             = new byte[1024];
		long bytesToCopy          = 0;
		long bytesWrote           = 0;
		long time                 = 0;
		int bytesRead             = 0;
		int progress              = 0;
		Bundle data               = null;
		Message msg               = null;
		cancel                    = false;
		
		bytesToCopy = checkItems(from, to, fromItems, toItems, items);
		
		if (bytesToCopy > 0)
			fileOperationNotify(fileOperation);
		
		try
		{
			int filesCount = fromItems.size();
			time           = System.currentTimeMillis();

			for (int i = 0; i < filesCount; i++)
			{
				fromItem = fromItems.get(i);
				toItem   = toItems.get(i);
				
				if (fromItem.isDirectory() && !cancel)
				{
					if (!toItem.exists())
						toItem.mkdir();
				}
				else if (!cancel)
				{					
					in   = new FileInputStream(fromItem);
					out  = new FileOutputStream(toItem);
	
					while ((bytesRead = in.read(buffer)) != -1 && !cancel)
					{
						out.write(buffer, 0, bytesRead);
						
						bytesWrote += bytesRead;
						
						if ((System.currentTimeMillis() - time) >= 1000)
						{
							time     = System.currentTimeMillis();
							progress = (int)(((float)bytesWrote / (float)bytesToCopy) * 100);
							
							fileOperation.setProgress(progress);
							
							fileOperationNotify(fileOperation);

							data = new Bundle();
							msg  = new Message();
							
							data.putString("fromTo", fileOperation.getTitle());
							data.putString("file", fromItem.getName());
							data.putString("folder", toPath);
							data.putInt("progress", progress);
							msg.setData(data);
							updateUi.sendMessage(msg);
						}
					}
					
					if (cancel)
					{
						out.close();
						toItem.delete();
					}
				}
			}
			
			cancel = false;
			data   = new Bundle();
			msg    = new Message();
			
			data.putString("fromTo", fileOperation.getTitle());
			data.putString("file", fromItem.getName());
			data.putString("folder", toPath);
			data.putInt("progress", 100);
			msg.setData(data);
			updateUi.sendMessage(msg);
			
			notificationManager.cancel(0);
			
			in.close();
			out.close();
		}
		catch (Exception e)
		{
		}
	}

	/**
	 * Delete items from local folder.
	 * 
	 * @param fileOperation Instance of FileOperation.
	 */
	private static void deleteFromLocal(FileOperation fileOperation)
	{
		File from                    = new File(fileOperation.getFrom().getPath());
		ArrayList<File> deleteItems  = new ArrayList<File>();
		ArrayList<String> items      = fileOperation.getItems();
		int itemsCount               = checkItems(from, deleteItems, items);
		Iterator<File> DeleteItemsIt = deleteItems.iterator();
		
		while (DeleteItemsIt.hasNext())
		{
			DeleteItemsIt.next().delete();
		}
		
		Bundle data = new Bundle();
		Message msg = new Message();
		
		data.putBoolean("refreshFolder", true);
		data.putString("folder", from.getAbsolutePath() + "/");
		msg.setData(data);
		updateUi.sendMessage(msg);
	}
	
	/**
	 * Check items for cut and copy.
	 * 
	 * @param from      Folder where the items are located.
	 * @param to        Folder where the items are going to be moved/copied.
	 * @param fromItems List of items in the source.
	 * @param toItems   List of items in the destination.
	 * @param items     Items to check.
	 * 
	 * @return Bytes to move/copy;
	 */
	private static long checkItems(File from, File to, ArrayList<File> fromItems,
								   ArrayList<File> toItems, ArrayList<String> items)
	{
		long bytesToCopy         = 0;
		String name              = "";
		String fromPath          = from.getAbsolutePath() + "/";
		String toPath            = to.getAbsolutePath() + "/";
		File fromItem            = null;
		File toItem              = null;
		boolean copyOk           = false;
		boolean skip             = false;
		Bundle data              = null;
		Message msg              = null;
		Iterator<String> itemsIt = items.iterator();
		
		while (itemsIt.hasNext())
		{
			name     = itemsIt.next();
			fromItem = new File(fromPath + name);
			toItem   = new File(toPath + name);
			
			if (toItem.exists())
			{			
				while (!copyOk)
				{
					existsAction = null;
					
					data = new Bundle();
					msg  = new Message();
					
					data.putBoolean(fromItem.isDirectory() ? "folderExists" : "fileExists", true);
					data.putString("file", newName == null ? name : newName);
					data.putString("folder", to.getName());
					msg.setData(data);
					updateUi.sendMessage(msg);
					
					while (existsAction == null);
					
					switch (existsAction)
					{
						case rename:
						{
							toItem = new File(toPath + newName);
							
							if (!toItem.exists())
								copyOk = true;
							
							break;
						}
						
						case skip:
						{
							skip   = true;
							copyOk = true;
							
							break;
						}
						
						case replace:
						{
							copyOk = true;
							
							break;
						}
						
						case merge:
						{						
							copyOk = true;
							
							break;
						}
					}
				}
				
				copyOk       = false;
				existsAction = null;
				newName      = null;
			}
			
			if (!skip)
			{
				fromItems.add(fromItem);
				toItems.add(toItem);
				
				if (fromItem.isDirectory())
				{
					ArrayList<String> subItems = new ArrayList<String>();
					Collections.addAll(subItems, fromItem.list());
					
					bytesToCopy += checkItems(fromItem, toItem, fromItems, toItems, subItems);
				}
				else
				{
					bytesToCopy += fromItem.length();
				}
			}
			else
			{
				skip = false;
			}
		}
			
		return bytesToCopy;
	}
	
	/**
	 * Check items for deletion.
	 * @param from        Folder where the items are going to be deleted.
	 * @param deleteItems List of the items to be deleted.
	 * @param items       Items to check.
	 * 
	 * @return Count of items to delete.
	 */
	private static int checkItems(File from, ArrayList<File> deleteItems, ArrayList<String> items)
	{
		File item                = null;
		String fromPath          = from.getAbsolutePath() + "/";
		Iterator<String> itemsIt = items.iterator();
		
		while (itemsIt.hasNext())
		{
			item = new File(fromPath + itemsIt.next());
			
			if (item.isDirectory() && item.canWrite())
			{
				ArrayList<String> subItems = new ArrayList<String>();
				Collections.addAll(subItems, item.list());
				
				int count = deleteItems.size() + subItems.size();
				checkItems(item, deleteItems, subItems);
				
				if (count <= deleteItems.size())
					deleteItems.add(item);
			}
			else if (item.canWrite())
			{
				deleteItems.add(item);
			}
		}
		
		return deleteItems.size();
	}
	
	/**
	 * Show file operation notification.
	 * 
	 * @param fileOperation Instance of FileOperation.
	 */
	private static void fileOperationNotify(FileOperation fileOperation)
	{
		int progress = fileOperation.getProgress();
		String title = fileOperation.getNotifyTitle();
		
		if (progress == 0)
		{
			long time            = System.currentTimeMillis();
			progressNotify       = new Notification(R.drawable.ic_notify, title, time);
			progressNotify.flags = progressNotify.flags | Notification.FLAG_ONGOING_EVENT;
			Intent intent        = new Intent(context, FileProgressActivity.class);
			String text          = fileOperation.getNotifyText() + ". 0%";
			
			progressNotify.setLatestEventInfo(context, title, text,
											  PendingIntent.getActivity(context, 0, intent, 0));
			notificationManager.notify(0, progressNotify);
		}
		else
		{	
			String text = fileOperation.getNotifyText() + ". " + Integer.toString(progress) + "%";
			
			progressNotify.setLatestEventInfo(context, title, text, progressNotify.contentIntent);
			notificationManager.notify(0, progressNotify);
		}
	}
	
	/**
	 * Show file operation queue notification.
	 */
	private static void fileOperationQueueNotify()
	{
		int queue    = fileOperations.size();
		String title = context.getString(R.string.fileOperationsInQueueTitle);
		String text  = Integer.toString(queue) + " "
					   + context.getString(R.string.fileOperationsInQueueText);
		
		if (queueNotify == null)
		{
			long time         = System.currentTimeMillis();
			queueNotify       = new Notification(R.drawable.ic_notify, title, time);
			queueNotify.flags = queueNotify.flags | Notification.FLAG_ONGOING_EVENT;
			
			queueNotify.setLatestEventInfo(context, title, text,
										   PendingIntent.getActivity(context, 0, new Intent(), 0));
			notificationManager.notify(1, queueNotify);
		}
		else if (queue > 0)
		{	
			queueNotify.setLatestEventInfo(context, title, text, queueNotify.contentIntent);
			notificationManager.notify(1, queueNotify);
		}
		else
		{
			notificationManager.cancel(1);

			queueNotify = null;
		}
	}
	
	/** 
	 * Class for file operation.
	 *
	 * @author Arttu Liimola
	 * @version 1.1
	 * date: 15.01.2012
	 */
	private class FileOperation
	{
		/** Source for items. */
		private Folder from;
		
		/** Destination for items. */
		private Folder to;
		
		/** Items. */
		private ArrayList<String> items;
		
		/** Action. */
		private Actions action;
		
		/** Progress. */
		private int progress;
		
		/** Dialog title. */
		private String title;
		
		/** Notification title. */
		private String notifyTitle;
		
		/** Notification text. */
		private String notifyText;
		
		/**
		 * Constructor.
		 * @param from   Source for items.
		 * @param to     Destination for items.
		 * @param items  Items.
		 * @param action Action.
		 */
		public FileOperation(Folder from, Folder to, ArrayList<String> items, Actions action)
		{
			this.from     = from;
			this.to       = to;
			this.items    = items;
			this.action   = action;
			this.progress = 0;
			
			switch (action)
			{
				case CutLocalToLocal:
				{
					this.title       = context.getString(R.string.movingFrom) + " "
									   + from.getName() + " "
									   + context.getString(R.string.movingTo) + " "
								       + to.getName();				
					this.notifyTitle = context.getString(R.string.movingItems);
					this.notifyText  = context.getString(R.string.movingFrom) + " "
							   		   + from.getName() + " "
							   		   + context.getString(R.string.movingTo) + " "
							   		   + to.getName();
					
					break;
				}
			
				case CopyLocalToLocal:
				{
					this.title 		 = context.getString(R.string.copyingFrom) + " "
									   + from.getName() + " "
									   + context.getString(R.string.copyingTo) + " "
									   + to.getName();
					this.notifyTitle = context.getString(R.string.copyingItems);
					this.notifyText  = context.getString(R.string.copyingFrom) + " "
							 		   + from.getName() + " "
							 		   + context.getString(R.string.copyingTo) + " "
							 		   + to.getName();
					
					break;
				}
			}
		}
		
		/**
		 * Get source for items.
		 * 
		 * @return Source for items.
		 */
		public Folder getFrom()
		{
			return from;
		}
		
		/**
		 * Get destination for items.
		 * 
		 * @return Destination for items.
		 */
		public Folder getTo()
		{
			return to;
		}
		
		/**
		 * Get items.
		 * 
		 * @return Items.
		 */
		public ArrayList<String> getItems()
		{
			return items;
		}
		
		/**
		 * Get action.
		 * 
		 * @return Action.
		 */
		public Actions getAction()
		{
			return action;
		}
		
		/**
		 * Get progress.
		 * 
		 * @return Progress.
		 */
		public int getProgress()
		{
			return progress;
		}
		
		/**
		 * Get dialog title.
		 * 
		 * @return Dialog title.
		 */
		public String getTitle()
		{
			return title;
		}
		
		
		/**
		 * Get notification title.
		 * 
		 * @return Notification title.
		 */
		public String getNotifyTitle()
		{
			return notifyTitle;
		}
		
		/**
		 * Get notification text.
		 * 
		 * @return Notification text.
		 */
		public String getNotifyText()
		{
			return notifyText;
		}
		
		/**
		 * Set progress.
		 * 
		 * @param progress Progress.
		 */
		public void setProgress(int progress)
		{
			this.progress = progress;
		}
	}
	
	/** 
	 * Class for updating Ui from thread.
	 *
	 * @author Arttu Liimola
	 * @version 1.2
	 * date: 16.01.2012
	 */
	private class UpdateUi extends Handler
	{
		
		/**
		 * Called when there is message to handle.
		 * 
		 * @param msg Message.
		 */
		@Override
		public void handleMessage(Message msg)
		{
			Bundle data = msg.getData();	
			
			if (data.getBoolean("fileExists"))
			{
				String file     = data.getString("file");
				fileExistDialog = new Dialog(context);
				fileExistDialog.setContentView(R.layout.file_exists_dialog);
				fileExistDialog.setCancelable(false);
				fileExistDialog.setTitle(R.string.fileExistsTitle);
				
				TextView text    = (TextView)fileExistDialog.findViewById(R.id.fileExistsText);
				Button rename    = (Button)fileExistDialog.findViewById(R.id.fileExistsRename);
				Button skip      = (Button)fileExistDialog.findViewById(R.id.fileExistsSkip);
				Button replace   = (Button)fileExistDialog.findViewById(R.id.fileExistsReplace);
				EditText newName = (EditText)fileExistDialog.findViewById(R.id.fileExistsNewName);
				
				text.setText(context.getString(R.string.fileExists1) + " " + file + " "
							 + context.getString(R.string.fileExists2) + " "
							 + data.getString("folder") + ".");
				rename.setOnClickListener(self);
				skip.setOnClickListener(self);
				replace.setOnClickListener(self);
				newName.setText(file);
				fileExistDialog.show();
			}
			else if (data.getBoolean("folderExists"))
			{
				String file     = data.getString("file");
				folderExistDialog = new Dialog(context);
				folderExistDialog.setContentView(R.layout.folder_exists_dialog);
				folderExistDialog.setCancelable(false);
				folderExistDialog.setTitle(R.string.folderExistsTitle);
				
				TextView text    = (TextView)folderExistDialog.findViewById(R.id.folderExistsText);
				Button rename    = (Button)folderExistDialog.findViewById(R.id.folderExistsRename);
				Button skip      = (Button)folderExistDialog.findViewById(R.id.folderExistsSkip);
				Button merge     = (Button)folderExistDialog.findViewById(R.id.folderExistsMerge);
				EditText newName = (EditText)folderExistDialog.findViewById(R.id.folderExistsNewName);
				
				text.setText(context.getString(R.string.folderExists1) + " " + file + " "
						 	 + context.getString(R.string.folderExists2) + " "
						 	 + data.getString("folder") + ".");
				rename.setOnClickListener(self);
				skip.setOnClickListener(self);
				merge.setOnClickListener(self);
				newName.setText(file);
				folderExistDialog.show();
			}
			else if (data.getBoolean("refreshFolder"))
			{
				FolderManager.refresh(data.getString("folder"));
			}
			else if (data.getBoolean("updateQueue"))
			{
				fileOperationQueueNotify();
			}
			else
			{
				FolderManager.refresh(data.getString("folder"));
				FileProgressActivity.setData(msg.getData());
			}
		}
	}
}
