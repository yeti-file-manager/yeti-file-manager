/*
*  ResourcesManager.java
*
*  Copyright information
*
*      Copyright (C) 2012-2015 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/** 
 * Class for resources.
 *
 * @author Arttu Liimola
 * @version 1.2
 * date: 04.02.2015
 */
public class ResourcesManager
{
	/** ENUM for versions. */
	private enum Versions
	{
		NO_VERSION,
		V1_0,
		V1_1,
		V1_2
	}
	
	/** Settings. */
	private static SharedPreferences settings;
	
	/** Map version string to ENUM version. */
	private static HashMap<String, Versions> versions;
	
	/** Interface to global information about an application environment. */
	private static Context context;
	
	/** Map MIME type to icon. */
	private static HashMap<String, Icon> icons;
	
	/** Map file to preview. */
	private static HashMap<String, Preview> previews;
	
	/** Available icons sizes. */
	private static ArrayList<Integer> iconSizes;
	
	/** Available icons. */
	private static ArrayList<String> availableIcons;
	
	/**
	 * Initializes variables of this class.
	 * 
	 * @param context Interface to global information about an application environment.
	 */
	public static void set(Context context)
	{
		ResourcesManager.settings = PreferenceManager.getDefaultSharedPreferences(context);
		ResourcesManager.context  = context;
		versions = new HashMap<String, Versions>();
		icons    = new HashMap<String, Icon>();
		previews = new HashMap<String, Preview>();
		
		versions.put("No version", Versions.NO_VERSION);
		versions.put("Version 1.0", Versions.V1_0);
		versions.put("Version 1.1", Versions.V1_1);
		versions.put("Version 1.2", Versions.V1_2);
		
		setIconSizes();
		setAvailableIconsList();
		setSettings(settings.getString("Version", "No version"));
	}
	
	/**
	 * Sets setting.
	 * 
	 * @param key   Name of setting.
	 * @param value Value of setting.
	 */
	public static <T> void setSetting(String key, T value)
	{
		SharedPreferences.Editor editor = settings.edit();
		
		if (value instanceof Integer)
		{
			editor.putInt(key, (Integer)value);
		}
		else if (value instanceof Float)
		{
			editor.putFloat(key, (Float)value);
		}
		else if (value instanceof Long)
		{
			editor.putLong(key, (Long)value);
		}
		else if (value instanceof Boolean)
		{
			editor.putBoolean(key, (Boolean)value);
		}
		else if (value instanceof String)
		{
			editor.putString(key, (String)value);
		}
		
		editor.commit();
	}
	
	/**
	 * Get setting.
	 * 
	 * @param key Name of setting.
	 * 
	 * @return Value of setting.
	 */
	public static <T> T getSetting(String key)
	{
		T setting;
		
		try
		{
			setting = (T)settings.getAll().get(key);
		}
		catch (ClassCastException e)
		{
			setting = null;
		}
		
		return setting;
	}
	
	/**
	 * Check if icon is available.
	 * 
	 * @param icon Path to the icon file.
	 * 
	 * @return True if icon is available, otherwise false.
	 */
	public static boolean isIconAvailabe(String icon)
	{
		return availableIcons.contains(icon);
	}
	
	/**
	 * Loads icon.
	 * 
	 * @param file File that needs an icon.
	 * @param type MIME type.
	 * 
	 * @return Returns icon.
	 */
	public static Bitmap loadIcon(File file, String type)
	{
		Icon icon = icons.get(type);
		
		if (icon == null)
		{
			icon = new Icon(file, type, context.getAssets(), context.getPackageManager());
			icons.put(type, icon);
		}

		return icon.getIcon();
	}
	
	/**
	 * Loads preview.
	 * 
	 * @param file File that need a preview.
	 * @param mime MIME type.
	 * 
	 * @return Returns preview.
	 */
	public static Bitmap loadPreview(File file, String mime)
	{	
		Preview preview = previews.get(file.getAbsolutePath());
		
		if (preview == null)
		{
			preview = new Preview(file, mime, context.getContentResolver());
			previews.put(file.getAbsolutePath(), preview);
		}

		return preview.getPreview();
	}
	
	/**
	 * Reloads icons and previews.
	 */
	public static void reloadIcons()
	{
		Iterator<Icon> iconsIt       = icons.values().iterator();
		Iterator<Preview> previewsIt = previews.values().iterator();
		AssetManager assets          = context.getAssets();
		ContentResolver cr           = context.getContentResolver();
		
		while (iconsIt.hasNext())
			iconsIt.next().reloadIcon(assets);
		
		while (previewsIt.hasNext())
			previewsIt.next().reloadPreview(cr);
	}
	
	/**
	 * Gets icon
	 * 
	 * @param iconFile Path to the icon file.
	 * @param size     Icon size.
	 * 
	 * @return Return icon if available, otherwise false.
	 */
	public static Bitmap getIcon(String iconFile, int size)
	{
		Bitmap icon     = null;
		String iconSize = Integer.toString(size);
		
		if (isIconAvailabe(iconFile))
		{
			iconFile = "icons/oxygen/" + iconSize + "x" + iconSize + "/" + iconFile;

			try
			{
				icon = BitmapFactory.decodeStream(context.getAssets().open(iconFile));
			}
			catch (IOException e)
			{
			}
		}
		
		return icon;
	}
	
	/**
	 * Sets settings for the current version.
	 * 
	 * @param version Version saved in settings.
	 */
	private static void setSettings(String version)
	{
		SharedPreferences.Editor editor = settings.edit();
		DisplayMetrics metrics          = context.getResources().getDisplayMetrics();
		int dip                         = TypedValue.COMPLEX_UNIT_DIP;
		
		switch (versions.get(version))
		{
			case NO_VERSION:
			{
				int iconSize = (int)TypedValue.applyDimension(dip, 48, metrics);
				int last     = iconSizes.size() - 1;
				int i        = last;

				while (iconSize < iconSizes.get(i))
					i--;
				
				if (i != last )
				{
					iconSize = (iconSize - iconSizes.get(i)) >= 8 ? iconSizes.get(i+1)
															  	  : iconSizes.get(i);
				}
				else
				{
					iconSize = iconSizes.get(i);
				}
				
				editor.putString("Version", "Version 1.0");
				editor.putString("DefaultPath", "/");
				editor.putString("SortByName", "[]");
				editor.putString("SortByDate", "[]");
				editor.putString("ShowsHidden", "[]");
				editor.putString("ShowsPreview", "[]");
				editor.putInt("FilenameSize", 7);
				editor.putInt("IconSize", iconSize);
			}
			
			case V1_0:
			{
				int mainViewChange   = (int)TypedValue.applyDimension(dip, 250, metrics);
				int minSelectionRect = (int)TypedValue.applyDimension(dip, 68, metrics);
				
				editor.putString("Version", "Version 1.1");
				editor.putInt("MainViewChange", mainViewChange);
				editor.putInt("MinSelectionRect", minSelectionRect);
			}
			
			case V1_1:
			{
				editor.putString("Version", "Version 1.2");
				editor.putString("Bookmarks", "{}");
			}
		}
		
		editor.commit();
	}
	
	/**
	 * Sets icon sizes.
	 */
	private static void setIconSizes()
	{
		iconSizes = new ArrayList<Integer>();
		
		iconSizes.add(32);
		iconSizes.add(48);
		iconSizes.add(64);
		iconSizes.add(80);
		iconSizes.add(96);
		iconSizes.add(112);
		iconSizes.add(128);
		iconSizes.add(144);
		iconSizes.add(160);
		iconSizes.add(176);
		iconSizes.add(192);
	}
	
	/**
	 * Sets available icons.
	 */
	private static void setAvailableIconsList()
	{
		availableIcons = new ArrayList<String>();
		
		availableIcons.add("mimetypes/application-msword.png");
		availableIcons.add("mimetypes/application-pdf.png");
		availableIcons.add("mimetypes/application-pgp-keys.png");
		availableIcons.add("mimetypes/application-pgp-signature.png");
		availableIcons.add("mimetypes/application-rar.png");
		availableIcons.add("mimetypes/application-rss+xml.png");
		availableIcons.add("mimetypes/application-vnd.android.package-archive.png");
		availableIcons.add("mimetypes/application-vnd.ms-excel.png");
		availableIcons.add("mimetypes/application-vnd.ms-powerpoint.png");
		availableIcons.add("mimetypes/application-vnd.oasis.opendocument.database.png");
		availableIcons.add("mimetypes/application-vnd.oasis.opendocument.formula.png");
		availableIcons.add("mimetypes/application-vnd.oasis.opendocument.graphics.png");
		availableIcons.add("mimetypes/application-vnd.oasis.opendocument.image.png");
		availableIcons.add("mimetypes/application-vnd.oasis.opendocument.spreadsheet-template.png");
		availableIcons.add("mimetypes/application-vnd.oasis.opendocument.spreadsheet.png");
		availableIcons.add("mimetypes/application-vnd.oasis.opendocument.text-master.png");
		availableIcons.add("mimetypes/application-vnd.oasis.opendocument.text-template.png");
		availableIcons.add("mimetypes/application-vnd.oasis.opendocument.text.png");
		availableIcons.add("mimetypes/application-vnd.openxmlformats-officedocument.wordprocessingml.document.png");
		availableIcons.add("mimetypes/application-vnd.stardivision.calc.png");
		availableIcons.add("mimetypes/application-vnd.stardivision.draw.png");
		availableIcons.add("mimetypes/application-vnd.stardivision.math.png");
		availableIcons.add("mimetypes/application-vnd.sun.xml.calc.png");
		availableIcons.add("mimetypes/application-vnd.sun.xml.calc.template.png");
		availableIcons.add("mimetypes/application-vnd.sun.xml.draw.png");
		availableIcons.add("mimetypes/application-vnd.sun.xml.draw.template.png");
		availableIcons.add("mimetypes/application-vnd.sun.xml.impress.png");
		availableIcons.add("mimetypes/application-vnd.sun.xml.impress.template.png");
		availableIcons.add("mimetypes/application-vnd.sun.xml.math.png");
		availableIcons.add("mimetypes/application-vnd.sun.xml.writer.global.png");
		availableIcons.add("mimetypes/application-vnd.sun.xml.writer.png");
		availableIcons.add("mimetypes/application-vnd.sun.xml.writer.template.png");
		availableIcons.add("mimetypes/application-x-abiword.png");
		availableIcons.add("mimetypes/application-x-bittorrent.png");
		availableIcons.add("mimetypes/application-x-cpio.png");
		availableIcons.add("mimetypes/application-x-debian-package.png");
		availableIcons.add("mimetypes/application-x-gnumeric.png");
		availableIcons.add("mimetypes/application-x-iso9660-image.png");
		availableIcons.add("mimetypes/application-x-kword.png");
		availableIcons.add("mimetypes/application-x-lha.png");
		availableIcons.add("mimetypes/application-x-object.png");
		availableIcons.add("mimetypes/application-x-shockwave-flash.png");
		availableIcons.add("mimetypes/application-x-tar.png");
		availableIcons.add("mimetypes/application-x-troff-man.png");
		availableIcons.add("mimetypes/application-xhtml+xml.png");
		availableIcons.add("mimetypes/application-zip.png");
		availableIcons.add("mimetypes/audio-midi.png");
		availableIcons.add("mimetypes/audio-prs.sid.png");
		availableIcons.add("mimetypes/audio-x-aiff.png");
		availableIcons.add("mimetypes/audio-x-generic.png");
		availableIcons.add("mimetypes/audio-x-pn-realaudio.png");
		availableIcons.add("mimetypes/audio-x-realaudio.png");
		availableIcons.add("mimetypes/audio-x-wav.png");
		availableIcons.add("mimetypes/image-svg+xml.png");
		availableIcons.add("mimetypes/image-x-generic.png");
		availableIcons.add("mimetypes/text-calendar.png");
		availableIcons.add("mimetypes/text-comma-separated-values.png");
		availableIcons.add("mimetypes/text-css.png");
		availableIcons.add("mimetypes/text-html.png");
		availableIcons.add("mimetypes/text-mathml.png");
		availableIcons.add("mimetypes/text-plain.png");
		availableIcons.add("mimetypes/text-rtf.png");
		availableIcons.add("mimetypes/text-x-bibtex.png");
		availableIcons.add("mimetypes/text-x-c++hdr.png");
		availableIcons.add("mimetypes/text-x-c++src.png");
		availableIcons.add("mimetypes/text-x-chdr.png");
		availableIcons.add("mimetypes/text-x-csrc.png");
		availableIcons.add("mimetypes/text-x-generic.png");
		availableIcons.add("mimetypes/text-x-haskell.png");
		availableIcons.add("mimetypes/text-x-java.png");
		availableIcons.add("mimetypes/text-x-pascal.png");
		availableIcons.add("mimetypes/text-x-tcl.png");
		availableIcons.add("mimetypes/text-x-tex.png");
		availableIcons.add("mimetypes/text-x-vcard.png");
		availableIcons.add("mimetypes/text-xml.png");
		availableIcons.add("mimetypes/unknown.png");
		availableIcons.add("mimetypes/video-x-generic.png");
		availableIcons.add("mimetypes/video-x-mng.png");
		availableIcons.add("places/folder-remote.png");
		availableIcons.add("places/folder-root.png");
		availableIcons.add("places/folder.png");
		availableIcons.add("places/network.png");
	}
	
}
