/*
*  BookmarkManager.java
*
*  Copyright information
*
*      Copyright (C) 2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/


package cx.ath.arggu;

import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Dialog;
import android.content.Context;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/** 
 * Class for managing bookmarks.
 *
 * @author Arttu Liimola
 * @version 1.0
 * date: 22.01.2012
 */
public class BookmarkManager implements OnItemClickListener, OnClickListener
{
	/** Instance of BookmarkManager. */
	private static BookmarkManager self;
	
	/** Interface to global information about an application environment. */
	private static Context context;
	
	/**	Places menu view. */
	private static LinearLayout placesMenu;
	
	/** ListView of bookmarks. */
	private static ListView bookmarksList;
	
	/** New bookmark button. */
	private static Button newBookmark;
	
	/** New bookmark dialog. */
	private static Dialog newBookmarkDialog;
	
	/** New bookmark name. */
	private static EditText newBookmarkName;
	
	/** New bookmark ok button. */
	private static Button newBookmarkOk;
	
	/** New bookmark cancel button. */
	private static Button newBookmarkCancel;
	
	/** List of bookmarks. */
	private static ArrayList<Bookmark> bookmarks;
	
	/** List of bookmarks names. */
	private static ArrayList<String> bookmarksNames;
	
	/**
	 * Initializes variables of this class.
	 * 
	 * @param context    Interface to global information about an application environment.
	 * @param placesMenu View of the places menu.
	 */
	public static void set(Context context, LinearLayout placesMenu)
	{
		self = new BookmarkManager();
		
		BookmarkManager.context    = context;
		BookmarkManager.placesMenu = placesMenu;
		
		bookmarksList          = (ListView)placesMenu.findViewById(R.id.bookmarksList);
		newBookmark            = (Button)placesMenu.findViewById(R.id.newBookmark);
		newBookmarkDialog      = new Dialog(context);
		newBookmarkDialog.setContentView(R.layout.new_bookmark_dialog);
		newBookmarkDialog.setTitle("Bookmark name");
		
		newBookmarkName        = (EditText)newBookmarkDialog.findViewById(R.id.newBookmarkName);
		newBookmarkOk          = (Button)newBookmarkDialog.findViewById(R.id.newBookmarkOk);
		newBookmarkCancel      = (Button)newBookmarkDialog.findViewById(R.id.newBookmarkCancel);
		bookmarks              = new ArrayList<Bookmark>();
		bookmarksNames         = new ArrayList<String>();
		String bookmark        = "";
		String path            = "";
		String icon            = "";
		JSONArray bookmarkData = null;
		
		try
		{
			JSONObject bM = new JSONObject(ResourcesManager.<String>getSetting("Bookmarks"));
			
			Iterator<String> bMIt = bM.keys();		
			
			while (bMIt.hasNext())
			{
				bookmark     = bMIt.next();
				bookmarkData = bM.getJSONArray(bookmark);
				path         = bookmarkData.getString(0);
				icon         = bookmarkData.getString(1);
				
				bookmarks.add(0, new Bookmark(bookmark, path, icon));
				bookmarksNames.add(0, bookmark);
			}
			
			path = Environment.getExternalStorageDirectory().getPath();
			
			bookmarks.add(0, new Bookmark("Sdcard", path, "sdcard"));
			bookmarksNames.add(0, "Sdcard");
			
			bookmarks.add(0, new Bookmark("Root", "/", "folder_root"));
			bookmarksNames.add(0, "Root");
		}
		catch (JSONException e)
		{
		}
		
		bookmarksList.setAdapter(new PlacesMenuAdapter(context, bookmarks));
		bookmarksList.setOnItemClickListener(self);
		newBookmark.setOnClickListener(self);
		newBookmarkOk.setOnClickListener(self);
		newBookmarkCancel.setOnClickListener(self);
	}

	/**
	 * Called when bookmark is clicked.
	 * 
	 * @param placesMenu Item's parent.
	 * @param itemView   Item.
	 * @param pos        Item's position.
	 * @param id         Item's id.
	 */
	@Override
	public void onItemClick(AdapterView<?> placesMenu, View itemView, int pos, long id)
	{
		String name = ((TextView)itemView.findViewById(R.id.bookmarkName)).getText().toString();
		
		TabManager.createTab(bookmarks.get(bookmarksNames.indexOf(name)).getPath());
		MainView.viewChanged();
	}

	/**
	 * Called when button is clicked.
	 * 
	 * @param v Button.
	 */
	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.newBookmark:
			{
				Folder folder = TabManager.getCurrentTab().getCurrentFolder();
				
				newBookmarkName.setText(folder.getName());
				newBookmarkDialog.show();
				
				break;
			}
			
			case R.id.newBookmarkOk:
			{
				String newBookmark = newBookmarkName.getText().toString();
				
				if (!bookmarksNames.contains(newBookmark))
				{
					newBookmarkDialog.dismiss();
					
					Iterator<String> bookmarksNamesIt = bookmarksNames.iterator();
					String bookmarkName               = "";
					int i                             = 0;
					boolean found                     = false;
					
					while (bookmarksNamesIt.hasNext() && !found)
					{
						bookmarkName = bookmarksNamesIt.next();
						
						if (!bookmarkName.equals("Root") && !bookmarkName.equals("Sdcard"))
						{
							if (newBookmark.compareToIgnoreCase(bookmarkName) < 0)
								found = true;
						}
						
						if (!found)
							i++;
					}
					
					
					Folder folder = TabManager.getCurrentTab().getCurrentFolder();
					String icon   = !folder.isRemote() ? "folder" : "folder_remote";
					
					bookmarks.add(i, new Bookmark(newBookmark, folder.getPath(), icon));
					bookmarksNames.add(i, newBookmark);
					bookmarksList.setAdapter(new PlacesMenuAdapter(context, bookmarks));
					MainView.viewChanged();
					
					JSONObject bM                    = new JSONObject();
					JSONArray bookmarkData           = null;
					Iterator<Bookmark> bookmarksIt   = bookmarks.iterator();
					Bookmark bookmark                = null;
		
					try
					{
						while (bookmarksIt.hasNext())
						{
							bookmark     = bookmarksIt.next();
							bookmarkName = bookmark.getName();
							
							if (!bookmarkName.equals("Root") && !bookmarkName.equals("Sdcard"))
							{
								bookmarkData = new JSONArray();
								
								bookmarkData.put(bookmark.getPath());
								bookmarkData.put(bookmark.getIcon());
								
								bM.put(bookmark.getName(), bookmarkData);
							}
						}
						
						ResourcesManager.setSetting("Bookmarks", bM.toString());
					}
					catch (JSONException e)
					{
					}
				}
				else
				{
					String msg = "There is already bookmark " + newBookmark;
					
					Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
				}
				
				break;
			}
			
			case R.id.newBookmarkCancel:
			{
				newBookmarkDialog.dismiss();
				
				break;
			}
		}
	}
}
