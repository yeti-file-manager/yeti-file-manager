/*
*  PlacesMenuAdapter.java
*
*  Copyright information
*
*      Copyright (C) 2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/** 
 * Class for bookmarks in ListView Adapter.
 *
 * @author Arttu Liimola
 * @version 1.0
 * date: 22.01.2012
 */
public class PlacesMenuAdapter extends BaseAdapter
{
	/** Interface to global information about an application environment. */
	private Context context;
	
	/** Bookmarks. */
	private ArrayList<Bookmark> bookmarks;
	
	/**
	 * Constructor.
	 * 
	 * @param context   Interface to global information about an application environment.
	 * @param bookmarks Bookmarks.
	 */
	public PlacesMenuAdapter(Context context, ArrayList<Bookmark> bookmarks)
    {
		this.context   = context;
		this.bookmarks = bookmarks;
    }
	
	/**
	 * Gets count of bookmarks.
	 * 
	 * @return Count of bookmarks.
	 */
	@Override
	public int getCount()
	{
		return bookmarks.size();
	}

	/**
	 * Gets bookmark.
	 * 
	 * @param position Position for the bookmark.
	 * 
	 * @return Returns bookmark.
	 */
	@Override
	public Object getItem(int position)
	{
		return bookmarks.get(position);
	}

	/**
	 * Gets bookmark's id.
	 * 
	 * @param position Position for the bookmark.
	 * 
	 * @return Returns bookmark's id.
	 */
	@Override
	public long getItemId(int position)
	{
		return position;
	}

	/**
	 * Get view for bookmark.
	 * 
	 * @param position    Position for the bookmark.
	 * @param convertView Recycled View instance.
	 * @param parent      View's parent.
	 * 
	 * @return Returns view.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view         = convertView;
		Bookmark bookmark = bookmarks.get(position);
		
		ImageView icon;
		TextView name;
		
		if (view == null)
		{
			LayoutInflater li = LayoutInflater.from(context);
            view = li.inflate(R.layout.places_menu_item, null);
		}

        icon = (ImageView) view.findViewById(R.id.bookmarkIcon);
        icon.setImageResource(context.getResources().getIdentifier(bookmark.getIcon(), "drawable",
        														   "cx.ath.arggu"));

        name = (TextView) view.findViewById(R.id.bookmarkName);
		name.setText(bookmark.getName());

		return view;
	}

}
