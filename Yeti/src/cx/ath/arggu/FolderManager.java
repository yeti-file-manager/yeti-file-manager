/*
*  FolderManager.java
*
*  Copyright information
*
*      Copyright (C) 2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;

/** 
 * Class for managing folders.
 *
 * @author Arttu Liimola
 * @version 1.1
 * date: 08.01.2012
 */
public class FolderManager
{
	/** List of folders. */
	private static HashMap<String, Folder> folders;
	
	/** List of folders that are sortedByName. */
	private static ArrayList<String> sortedByName;
	
	/** List of folders that are sortedByDate. */
	private static ArrayList<String> sortedByDate;
	
	/** List of folders in which hidden items are shown. */
	private static ArrayList<String> showsHidden;
	
	/** List of folders in which preview are shown. */
	private static ArrayList<String> showsPreview;
	
	/**
	 * Initializes variables of this class.
	 */
	public static void set()
	{
		folders = new HashMap<String, Folder>();
		
		try
		{
			JSONArray sN = new JSONArray(ResourcesManager.<String>getSetting("SortByName"));
			JSONArray sD = new JSONArray(ResourcesManager.<String>getSetting("SortByDate"));
			JSONArray sH = new JSONArray(ResourcesManager.<String>getSetting("ShowsHidden"));
			JSONArray sP = new JSONArray(ResourcesManager.<String>getSetting("ShowsPreview"));
			
			sortedByName = new ArrayList<String>();
			sortedByDate = new ArrayList<String>();
			showsHidden  = new ArrayList<String>();
			showsPreview = new ArrayList<String>();
			
			for (int i = 0; i < sN.length(); i++)
				sortedByName.add(sN.getString(i));
			
			for (int i = 0; i < sD.length(); i++)
				sortedByDate.add(sD.getString(i));
			
			for (int i = 0; i < sH.length(); i++)
				showsHidden.add(sH.getString(i));
			
			for (int i = 0; i < sP.length(); i++)
				showsPreview.add(sP.getString(i));
		}
		catch (JSONException e)
		{
		}
	}
	
	/**
	 * Create instance of Folder-class.
	 * 
	 * @param path            Path of the folder.
	 * @param showOnlyFolders If true only folders are shown in the items.
	 * 
	 * @return Instance of Folder-class.
	 */
	public static Folder createFolder(String path, boolean showOnlyFolders)
	{
		Folder folder = folders.get(path);
		
		if (folder == null)
		{
			folder = new Folder(path, showOnlyFolders);
			folders.put(path, folder);
		}
		
		return folder;
	}
	
	/**
	 * Refresh folder.
	 * 
	 * @param path Path of the folder.
	 */
	public static void refresh(String path)
	{
		String secondPath = path.startsWith("/mnt") ? path.substring(4) : "/mnt" + path;
		
		folders.get(path).refresh();
		
		if (folders.containsKey(secondPath))
			folders.get(secondPath).refresh();
	}
	
	/**
	 * Reloads icons.
	 */
	public static void reloadIcons()
	{
		Iterator<Folder> foldersIt = folders.values().iterator();
		
		while (foldersIt.hasNext())
			foldersIt.next().reloadIcons();
	}
	
	/**
	 * Resets filename size.
	 */
	public static void resetFilenameSize()
	{
		Iterator<Folder> foldersIt = folders.values().iterator();
		
		while (foldersIt.hasNext())
			foldersIt.next().reloadIcons();
	}
	
	/**
	 * Get sorting from folder.
	 * 
	 * @param path Path of the folder.
	 * 
	 * @return Sorting of the folder.
	 */
	public static String getSorting(String path)
	{
		String sortBy = "type";
		
		sortBy = sortedByName.contains(path) ? "name" : sortBy;
		sortBy = sortedByDate.contains(path) ? "date" : sortBy;
		
		return sortBy;
	}
	
	/**
	 * Get folder.
	 * 
	 * @param path Path of the folder.
	 * 
	 * @return Folder.
	 */
	public static Folder getFolder(String path)
	{
		return folders.get(path);
	}
	
	/**
	 * Check if folder shows hidden items.
	 * @param path Path of the folder.
	 * 
	 * @return True if folder shows hidden items.
	 */
	public static boolean showsHidden(String path)
	{
		return showsHidden.contains(path);
	}
	
	/**
	 * Check if folder shows previews.
	 * @param path Path of the folder.
	 * 
	 * @return True if folder shows previews.
	 */
	public static boolean showsPreviews(String path)
	{
		return showsPreview.contains(path);
	}
	
	/**
	 * Sets sorting.
	 * 
	 * @param path Folder path.
	 * @param sortBy Specifies how items are sorted.
	 */
	public static void setSorting(String path, String sortBy)
	{		
		ArrayList<String> sortList    = null;
		ArrayList<String> newSortList = null;
		String settingName            = "";
		String newSettingName         = "";
		String oldSortBy              = folders.get(path).getSorting();
		String secondPath             = path.startsWith("/mnt") ? path.substring(4) : "/mnt" + path;
		
		if (oldSortBy.equals("name"))
		{
			sortList    = sortedByName;
			settingName = "SortByName";
		}
		else if (oldSortBy.equals("date"))
		{
			sortList    = sortedByDate;
			settingName = "SortByDate";
		}
		
		if (sortBy.equals("name"))
		{
			newSortList    = sortedByName;
			newSettingName = "SortByName";
		}
		else if (sortBy.equals("date"))
		{
			newSortList    = sortedByDate;
			newSettingName = "SortByDate";
		}
		
		if (sortList != null)
		{
			sortList.remove(path);
			sortList.remove(secondPath);
			
			JSONArray sL = new JSONArray(sortList);
			ResourcesManager.setSetting(settingName, sL.toString());
		}
		
		if (newSortList != null)
		{
			newSortList.add(path);
			newSortList.add(secondPath);
			
			JSONArray sL = new JSONArray(newSortList);
			ResourcesManager.setSetting(newSettingName, sL.toString());
		}
		
		folders.get(path).setSorting(sortBy);
		
		if (folders.containsKey(secondPath))
			folders.get(secondPath).setSorting(sortBy);
	}
	
	/**
	 * Sets show hidden.
	 * 
	 * @param path Folder path.
	 * @param showHidden Specifies if hidden items are shown.
	 */
	public static void setShowHidden(String path, boolean showHidden)
	{
		String secondPath = path.startsWith("/mnt") ? path.substring(4) : "/mnt" + path;
		
		if (showHidden)
		{
			showsHidden.add(path);
			showsHidden.add(secondPath);
		}
		else
		{
			showsHidden.remove(path);
			showsHidden.remove(secondPath);
		}
		
		JSONArray sH = new JSONArray(showsHidden);
		ResourcesManager.setSetting("ShowsHidden", sH.toString());
		
		folders.get(path).setShowHidden(showHidden);
		
		if (folders.containsKey(secondPath))
			folders.get(secondPath).setShowHidden(showHidden);
	}
	
	/**
	 * Sets show previews.
	 * 
	 * @param path Folder path.
	 * @param showPreview Specifies if previews are shown.
	 */
	public static void setShowPreviews(String path, boolean showPreview)
	{
		String secondPath = path.startsWith("/mnt") ? path.substring(4) : "/mnt" + path;
		
		if (showPreview)
		{
			showsPreview.add(path);
			showsPreview.add(secondPath);
		}
		else
		{
			showsPreview.remove(path);
			showsPreview.remove(secondPath);
		}
		
		JSONArray sP = new JSONArray(showsPreview);
		ResourcesManager.setSetting("ShowsPreview", sP.toString());
		
		folders.get(path).setShowPreviews(showPreview);
		
		if (folders.containsKey(secondPath))
			folders.get(secondPath).setShowPreviews(showPreview);
	}
}
