/*
*  FolderView.java
*
*  Copyright information
*
*      Copyright (C) 2011-2012 Arttu Liimola <arttu.liimola@gmail.com>
*
*  License
*
*      This file is part of Yeti.
*
*      This program is free software: you can redistribute it and/or modify
*      it under the terms of the GNU General Public License as published by
*      the Free Software Foundation, either version 3 of the License, or
*      (at your option) any later version.
*
*      This program is distributed in the hope that it will be useful,
*      but WITHOUT ANY WARRANTY; without even the implied warranty of
*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*      GNU General Public License for more details.
*
*      You should have received a copy of the GNU General Public License
*      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

package cx.ath.arggu;

import java.util.ArrayList;
import java.util.Iterator;
import android.content.Context;
/* Uncomment this for Android versions without hardware acceleration 
import android.graphics.Canvas;*/
import android.util.AttributeSet;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View;

/** 
 * Class for folders items view.
 *
 * @author Arttu Liimola
 * @version 1.2
 * date: 15.01.2012
 */
public class FolderView extends GridView
{

	/** Uncomment these three variables for Android versions without hardware acceleration */
	/** View's scale.
	private float scale; */
	
	/** View's alpha.
	private int alpha; */
	
	/** View's x position.
	private int toX; */
	
	/**	Id of not selected drawable. */
	private int notSelectedDrawable;
	
	/**	Selected items. */
	private ArrayList<String> selectedItems;
	
	/**
	 * Constructor.
	 * 
	 * @param context Interface to global information about an application environment.
	 * @param attrs   Collection of attributes from XML-file.
	 */
	public FolderView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		
		/** Uncomment these three variables for Android versions without hardware acceleration 
		scale = 1f;
		alpha = 255;
		toX   = 0;*/
		
		
        notSelectedDrawable = R.drawable.folder_view_not_selected;
		selectedItems       = new ArrayList<String>();
		
		/** Comment this for Android versions without hardware acceleration */
		this.setLayerType(View.LAYER_TYPE_HARDWARE, null);
	}

	/**
	 * Set view's scale, alpha and x position.
	 * 
	 * @param scale View's scale.
	 * @param alpha View's alpha.
	 * @param toX   View's x position.
	 */
	public void setTransformation(float scale, float alpha, int toX)
	{
		/** Uncomment these three variables for Android versions without hardware acceleration
		this.scale = scale;
		this.alpha = (int)(alpha * 255);
		this.toX   = toX;*/
		
		/** Comment these for Android versions without hardware acceleration */
		setAlpha(alpha);
		setScaleX(scale);
		setScaleY(scale);
		setTranslationX(toX);
		
		/** Uncomment this for Android versions without hardware acceleration
		invalidate(); */
	}
	
	/**
	 * Selects items.
	 * 
	 * @param left   Left position of selection.
	 * @param top    Top position of selection.
	 * @param right  Right position of selection.
	 * @param bottom Bottom position of selection.
	 */
	public void selection(int left, int top, int right, int bottom)
	{
		int count        = getChildCount();
		int cLeft        = 0;
		int cTop         = 0;
		int cRight       = 0;
		int cBottom      = 0;
		boolean leftOk   = false;
		boolean topOk    = false;
		boolean rightOk  = false;
		boolean bottomOk = false;
		View child       = null;
		String item      = "";
		
		selectedItems.clear();
		
		clearSeletion();
		
		for (int i = 0; i < count; i++)
		{
			child    = getChildAt(i);
			cLeft    = child.getLeft();
			cTop     = child.getTop();
			cRight   = child.getRight();
			cBottom  = child.getBottom();
			leftOk   = (cLeft > left && cLeft < right) || (left > cLeft && right < cRight);
			topOk    = (cTop > top && cTop < bottom) || (top > cTop && top < cBottom);
			rightOk  = (cRight > left && cRight < right) || (right > cLeft && right < cRight);
			bottomOk = (cBottom > top && cBottom < bottom) || (bottom > cTop && bottom < cBottom);
	
			if ((leftOk && (topOk || bottomOk)) || (rightOk && (topOk || bottomOk)))
			{
				item = ((TextView)((LinearLayout)child).getChildAt(1)).getText().toString();
				
				selectedItems.add(item);
				((GridItemAdapter)getAdapter()).getItemByName(item).setSelected(true);
			}
		}
		
		invalidateViews();
	}
	
	/**
	 * Clears selection.
	 */
	public void clearSeletion()
	{
		selectedItems.clear();
		
		Iterator<Item> itemsIt = ((GridItemAdapter)getAdapter()).getItems().iterator();
		
		while(itemsIt.hasNext())
		{
			itemsIt.next().setSelected(false);
		}
		
		int count = getChildCount();
		
		for (int i = 0; i < count; i++)
		{
			getChildAt(i).setBackgroundResource(notSelectedDrawable);
		}
	}
	
	/**
	 * Get selected items.
	 * 
	 * @return Selected items.
	 */
	public ArrayList<String> getSelectedItems()
	{
		return selectedItems;
	}
	
	/**
	 * Draws view.
	 * 
	 * @param canvas The Canvas to which the view will be drawn.
	 */
	//@Override
	/** Uncomment this for Android versions without hardware acceleration
	public void onDraw(Canvas canvas)
	{
		if (alpha < 255)
			canvas.saveLayerAlpha(0, 0, getWidth(), getHeight(), alpha,
					              Canvas.HAS_ALPHA_LAYER_SAVE_FLAG
					              | Canvas.CLIP_TO_LAYER_SAVE_FLAG);
		
		if (scale < 1f)
			canvas.scale(scale, scale);
		
		if (toX > 0)
			canvas.translate(toX, 0);
		
		super.onDraw(canvas);
	} */
}
