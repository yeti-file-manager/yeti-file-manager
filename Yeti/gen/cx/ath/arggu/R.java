/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package cx.ath.arggu;

public final class R {
    public static final class array {
        public static final int FolderSettingsSortBy=0x7f050000;
        public static final int FolderSettingsSortByValues=0x7f050001;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int folderViewSelector=0x7f060000;
        public static final int selectingRect=0x7f060001;
        public static final int selectingRectBorders=0x7f060002;
    }
    public static final class drawable {
        public static final int folder=0x7f020000;
        public static final int folder_remote=0x7f020001;
        public static final int folder_root=0x7f020002;
        public static final int folder_view_not_selected=0x7f020003;
        public static final int folder_view_selected=0x7f020004;
        public static final int folder_view_selector=0x7f020005;
        public static final int ic_launcher=0x7f020006;
        public static final int ic_menu_add=0x7f020007;
        public static final int ic_menu_close=0x7f020008;
        public static final int ic_menu_folder_options=0x7f020009;
        public static final int ic_menu_refresh=0x7f02000a;
        public static final int ic_menu_settings=0x7f02000b;
        public static final int ic_notify=0x7f02000c;
        public static final int network=0x7f02000d;
        public static final int new_bookmark=0x7f02000e;
        public static final int progress_horizontal=0x7f02000f;
        public static final int sdcard=0x7f020010;
    }
    public static final class id {
        public static final int bookmarkIcon=0x7f0a001d;
        public static final int bookmarkName=0x7f0a001e;
        public static final int bookmarksList=0x7f0a0016;
        public static final int closeTab=0x7f0a0027;
        public static final int copyItems=0x7f0a002b;
        public static final int createFolder=0x7f0a0029;
        public static final int createFolderCancel=0x7f0a0003;
        public static final int createFolderInto=0x7f0a0000;
        public static final int createFolderName=0x7f0a0001;
        public static final int createFolderOk=0x7f0a0002;
        public static final int cutItems=0x7f0a002a;
        public static final int deleteItems=0x7f0a002d;
        public static final int fileExistsNewName=0x7f0a0008;
        public static final int fileExistsRename=0x7f0a0005;
        public static final int fileExistsReplace=0x7f0a0007;
        public static final int fileExistsSkip=0x7f0a0006;
        public static final int fileExistsText=0x7f0a0004;
        public static final int fileManagement=0x7f0a0017;
        public static final int fileProgressDialogBar=0x7f0a000b;
        public static final int fileProgressDialogCancel=0x7f0a000c;
        public static final int fileProgressDialogFile=0x7f0a000a;
        public static final int fileProgressDialogFromTo=0x7f0a0009;
        public static final int folderExistsMerge=0x7f0a0010;
        public static final int folderExistsNewName=0x7f0a0011;
        public static final int folderExistsRename=0x7f0a000e;
        public static final int folderExistsSkip=0x7f0a000f;
        public static final int folderExistsText=0x7f0a000d;
        public static final int folderOptions=0x7f0a0025;
        public static final int icon=0x7f0a0012;
        public static final int name=0x7f0a0013;
        public static final int newBookmark=0x7f0a0015;
        public static final int newBookmarkCancel=0x7f0a001c;
        public static final int newBookmarkName=0x7f0a001a;
        public static final int newBookmarkOk=0x7f0a001b;
        public static final int newTab=0x7f0a0026;
        public static final int openSettings=0x7f0a0024;
        public static final int pasteItems=0x7f0a002c;
        public static final int placesMenu=0x7f0a0014;
        public static final int refreshFolder=0x7f0a0028;
        public static final int selectionView=0x7f0a0019;
        public static final int settingsFilenameSizeImage=0x7f0a001f;
        public static final int settingsFilenameSizeSlider=0x7f0a0020;
        public static final int settingsIconSizeImage=0x7f0a0021;
        public static final int settingsIconSizeSlider=0x7f0a0022;
        public static final int settingsMainViewChangeSlider=0x7f0a0023;
        public static final int tabHost=0x7f0a0018;
    }
    public static final class layout {
        public static final int create_folder=0x7f030000;
        public static final int file_exists_dialog=0x7f030001;
        public static final int file_progress_dialog=0x7f030002;
        public static final int folder_exists_dialog=0x7f030003;
        public static final int grid_item=0x7f030004;
        public static final int icon_view=0x7f030005;
        public static final int main=0x7f030006;
        public static final int new_bookmark_dialog=0x7f030007;
        public static final int places_menu_item=0x7f030008;
        public static final int settings_filename_size=0x7f030009;
        public static final int settings_icon_size=0x7f03000a;
        public static final int settings_main_view_change=0x7f03000b;
    }
    public static final class menu {
        public static final int main_menu=0x7f090000;
    }
    public static final class string {
        public static final int app_name=0x7f07000b;
        public static final int cancelFileOperation=0x7f070032;
        public static final int cannotCreateFolder=0x7f070019;
        public static final int cannotOpenFile=0x7f07001c;
        public static final int cannotReadFolder=0x7f070014;
        public static final int closeTab=0x7f07000d;
        public static final int copyItems=0x7f070011;
        public static final int copyingFrom=0x7f07002e;
        public static final int copyingItems=0x7f07002d;
        public static final int copyingTo=0x7f07002f;
        public static final int createFolder=0x7f07000f;
        public static final int createNewFolderCancel=0x7f070017;
        public static final int createNewFolderMsg=0x7f070015;
        public static final int createNewFolderOk=0x7f070016;
        public static final int cutItems=0x7f070010;
        public static final int deleteItems=0x7f070013;
        public static final int fileExists1=0x7f07001f;
        public static final int fileExists2=0x7f070020;
        public static final int fileExistsRename=0x7f070021;
        public static final int fileExistsReplace=0x7f070023;
        public static final int fileExistsSkip=0x7f070022;
        public static final int fileExistsTitle=0x7f07001e;
        public static final int fileOperationsInQueueText=0x7f070031;
        public static final int fileOperationsInQueueTitle=0x7f070030;
        public static final int folderAlreadyExists=0x7f07001a;
        public static final int folderCreated=0x7f070018;
        public static final int folderExists1=0x7f070025;
        public static final int folderExists2=0x7f070026;
        public static final int folderExistsMerge=0x7f070029;
        public static final int folderExistsRename=0x7f070027;
        public static final int folderExistsSkip=0x7f070028;
        public static final int folderExistsTitle=0x7f070024;
        public static final int folderNameEmpty=0x7f07001b;
        public static final int folderOptions=0x7f070001;
        public static final int itemDoesNotExist=0x7f07001d;
        public static final int movingFrom=0x7f07002b;
        public static final int movingItems=0x7f07002a;
        public static final int movingTo=0x7f07002c;
        public static final int newTab=0x7f07000c;
        public static final int openSettings=0x7f070000;
        public static final int pasteItems=0x7f070012;
        public static final int refreshFolder=0x7f07000e;
        public static final int settingsDefaultFolderTitle=0x7f070004;
        public static final int settingsFilenameSizeTitle=0x7f070008;
        public static final int settingsFolderShowHiddenTitle=0x7f070007;
        public static final int settingsFolderShowPreviewsTitle=0x7f070006;
        public static final int settingsFolderSortByTitle=0x7f070005;
        public static final int settingsFolderTitle=0x7f070003;
        public static final int settingsIconSizeTitle=0x7f070009;
        public static final int settingsMainViewChangeTitle=0x7f07000a;
        public static final int settingsTitle=0x7f070002;
    }
    public static final class style {
        public static final int bookmarkName=0x7f080008;
        public static final int fileProgressNotifyText=0x7f08000b;
        public static final int gridItemName=0x7f08000a;
        public static final int iconView=0x7f080009;
        public static final int settingsFilenameSizeLayout=0x7f080000;
        public static final int settingsFilenameSizeSlider=0x7f080002;
        public static final int settingsFilenameSizeText=0x7f080001;
        public static final int settingsIconSizeImage=0x7f080004;
        public static final int settingsIconSizeLayout=0x7f080003;
        public static final int settingsIconSizeSlider=0x7f080005;
        public static final int settingsMainViewChangeLayout=0x7f080006;
        public static final int settingsMainViewChangeSlider=0x7f080007;
    }
    public static final class xml {
        public static final int folder_options_preference=0x7f040000;
        public static final int settings_preference=0x7f040001;
    }
}
